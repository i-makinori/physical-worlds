(in-package :physical-worlds)


;;;;

(defun describe-object-to-string (object)
  (let* ((str1 (make-array '(0) :element-type 'base-char :fill-pointer 0 :adjustable t)))
    (with-output-to-string (s str1)
      (describe object s))
    str1))

;;;;

(defun safe-div (number &rest more-numbers)
  (handler-case (apply #'/ number more-numbers)
    (division-by-zero (c) c 0)))

;;;; set theory

(defun contains-p (super smaller &key (test #'eql))
  (every #'(lambda (s1)
             (some 
              #'(lambda (s2) (funcall test s1 s2))
              super))
         smaller))

(defun equal-set-p (set1 set2)
  (and (contains-p set1 set2)
       (contains-p set2 set1)))


;;;; 2d-point

(defun 2d-pointp (point?)
  (and (consp point?)
       (numberp (car point?))
       (numberp (cdr point?))))


(defun 2d-point (x y)
  (cons x y)
)

(defun 2d-point-x (point)
  (car point))

(defun 2d-point-y (point)
  (cdr point))

(defun 2d-point-delta (point-1 point-2)
  (if (and (consp point-1) (consp point-2))
      (2d-point (- (2d-point-x point-1) (2d-point-x point-2))
                (- (2d-point-y point-1) (2d-point-y point-2)))
      nil))


