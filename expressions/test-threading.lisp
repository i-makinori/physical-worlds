(in-package :physical-worlds)

;; threading test


(ql:quickload :bordeaux-threads)


(defun fib (n)
  (cond ((= n 0) 1)
        ((= n 1) 1)
        ((< n 0) 0)
        (t (+ (fib (- n 2)) (fib (- n 1))))))

(defun test-thread ()
  (let* ((bt:*default-special-bindings* `((*trace-output* . ,*trace-output*)))
         (n 24)
         (ths (mapcar (lambda (i)
                        (declare (ignore i))
                        (bt:make-thread (lambda ()
                                          (time (fib 40)))))
                      (bordeaux-threads::iota n))))
    (mapcar #'bt:join-thread ths)))


;; lparallel

;; https://z0ltan.wordpress.com/2016/09/09/basic-concurrency-and-parallelism-in-common-lisp-part-4a-parallelism-using-lparallel-fundamentals/

(ql:quickload :lparallel)

(defun show-kernel-info ()
           (let ((name (lparallel:kernel-name))
                 (count (lparallel:kernel-worker-count))
                 (context (lparallel:kernel-context))
                 (bindings (lparallel:kernel-bindings)))
             (format t "Kernel name = ~a~%" name)
             (format t "Worker threads count = ~d~%" count)
             (format t "Kernel context = ~a~%" context)
             (format t "Kernel bindings = ~a~%" bindings)))

(lparallel:make-kernel 8 :name "custom-kernel")



(defparameter *n-workers* 4)
(setf lparallel:*kernel* (lparallel:make-kernel *n-workers*))

(defun shutdown ()
  (lparallel:end-kernel :wait t))

(defun calculate-square (n)
  (let* ((channel (lparallel:make-channel))
         (res nil))
    (lparallel:submit-task channel #'(lambda (x)
                                       (* x x))
                           n)
    (setf res (lparallel:receive-result channel))
    (format t "Square of ~d = ~d~%" n res)))

(defun test-basic-channel-multiple-tasks ()
  (let ((channel (lparallel:make-channel))
        (res '()))
    (lparallel:submit-task channel #'(lambda (x) (* x x)) 10)
    (lparallel:submit-task channel #'(lambda (x) (* x x x)) 10)
    (lparallel:submit-task channel #'(lambda (x) (* x x x x)) 10)
    (dotimes (i 3 res)
      (push (lparallel:receive-result channel) res))))

(defun test-queue-properties ()
  (let ((queue (lparallel.queue:make-queue :fixed-capacity 5)) ;; vector queue
        (iter 0))
    (loop
      when (lparallel.queue:queue-full-p queue)
      do (return)
      do ;;(lparallel.queue:push-queue (random 100) queue))
         (lparallel.queue:push-queue iter queue)
      do (incf iter))
    (print (lparallel.queue:queue-full-p queue))
    (loop
      when (lparallel.queue:queue-empty-p queue)
      do (return)
      do (print (lparallel.queue:pop-queue queue)))
    (print (lparallel.queue:queue-empty-p queue))
    nil))
    
(defun test-basic-queue ()
  (let ((queue (lparallel.queue:make-queue)) ;; cons queue
        (channel (lparallel:make-channel)) ;; no limited channel
        (res '()))
    (lparallel:submit-task channel 
                           #'(lambda ()
                               (loop for entry = (lparallel.queue:pop-queue queue)
                                     when (lparallel.queue:queue-empty-p queue)
                                     do (return)
                                     do (sleep 1)
                                     do (print (* entry entry))
                                     do (push (* entry entry) res))))
    (print "task A")
    (dotimes (i 10)
      (lparallel.queue:push-queue i queue))
    (print "task B")
    (lparallel:receive-result channel)
    (print "task C")
    (format t "~%~{~A ~}~%" res)))

(defun test-kill-all-tasks ()
  (let ((channel (lparallel:make-channel))
        (stream *query-io*))
    (setq lparallel:*task-category* 'hoge)
    (print lparallel:*task-category*)
    (dotimes (i 10)
      (lparallel:submit-task channel #'(lambda (x)
                                         (sleep (random 10))
                                         (format stream "~d~%" (* x x)))
                             (random 10)))
    (sleep (random 2))
    ;;(lparallel:kill-tasks :default)))

    (lparallel:kill-tasks 'hoge)
    (lparallel:receive-result channel))
  ;; number of only workers are maximam killed.
  ;; other wise are remained
  
)


    

    

;;

(defstruct manager 
  (stoppingp nil)
  (dt 0.1))

(defun loop-thread (manager)
  manager
  (format t "start loop~%")
  (let* ((counter 0))
    (loop while (not (manager-stoppingp manager))
          ;while (not (zerop (random 10)))
          do (format t "C:~A " counter)
             (incf counter)
             (manager-func manager)
             (format t "~%")
          )
    (format t "end loop~%")
    counter
  ))

(defun manager-func (manager)
  (let ((rand-num (random 5)))
    (format t "R:~A " rand-num)
    (setf (manager-stoppingp manager)
          (cond ((zerop rand-num) t)
                (t nil)))))

(defun test-loop-thread ()
  (loop-thread (make-manager))
)

;;

(defun classical-mechanics-time-loop ()
  ;; in the real time mode (Game mode).
  ;; updation loop for Newton Physics.
  ;; delta_time is absolute
  #|
  (loop
    while (not endsignal)
    do (progn
         ;; restriction: Display refers Model's memory.
         ;; 
         
         j. handle Key ;; from mouse, keyboard, gamepad, and any input devices.
         .. manipulate Model by Key.
         .. manipulate Misplay by Key.
         k. let (Display_time <-- Model_time)
         .. let t_before <-- get Real's time
         l. calculate Model(time+d_t) ; update World
         l. if not busy(display) => 
            .. thread(display model(time+frame_skip_able|...|time+d_t|time)))
            if busy(display) && frame-skip-able =>
            .. decf frame_skip_able
            .. nil ;; display thread is running
            if busy(display) && not frame-skip-able => 
            .. flush() ; swap_buffer()
            .. thread(display model(time+frame_skip_able|...|time+d_t|time))
         m. let t_after <-- get Real's time
         .. 
)
    |#
  )

;;


(defun time-test (sec)
  (let ((time1 0) (time2 0))
    (progn (print (setf time1 
                        (float (/ (get-internal-real-time) internal-time-units-per-second))))
           (format t "~%~d sec waiting" sec)
           (sleep sec) 
           (print (setf time2 
                        (float (/ (get-internal-real-time) internal-time-units-per-second))))
           (print (- time2 time1)))))
  


(defun make-an-object ()
  
)

(defun make-obj-list (n-objs)
  (loop for i from 1 to n-objs
        collect i)
)

(defparameter **test-maked-obj**
  (make-obj-list 10)
  )

(defclass test-threading-window (simulator-window)
  ((physical-objects :initform (make-obj-list 100))
   )
  )



