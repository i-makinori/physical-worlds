
(in-package :physical-worlds)


;;;; terrain

(defun terrain-noise (n-dimension seed)
  seed
  (let ((tmp-array
          (make-array (list n-dimension n-dimension))))
    (loop for iy from 0 to (1- n-dimension)
          do (loop for ix from 0 to (1- n-dimension)
                   do (setf (aref tmp-array iy ix)
                            (random 1.00))))
    tmp-array))

(defun diyamond-square ())

#|
;; clocksise-symmetric-list (CSL)
((diff/diff_n) (CSL n))
0: 2 0a 0b 2b 1a 
1: 2 3a 2b 0b 2a
2: 2 1a 3b 1b 0a
3: 2 2a 1b 3b 3a
|#


(defun make-terrain ()
  (multiple-value-call
      #'vertex-and-triangle-indexes-to-triangle-list
    (multiple-value-call
        #'whole-3split-triangles
      (icosahedron-vertex-index-list)
      :radius 1.7)));;(vec3-magnitude (vector-3d 0 1.0 *phi*)))))


(defun project-terrain (terrain-planes)
  terrain-planes
  (mapcar #'(lambda (triangle)
              (gl:with-pushed-matrix
                (gl:color 0.5 0.0 0.0)
                (gl:color 0.1 0.1 0.1)
                (gl:material :front-and-back :ambient *color-cyan*)
                
                (projection triangle)
                )
              ;(projection-wire plane)
              ;(project-vector *zero-vec3* (triangle->normal plane))
              )
          terrain-planes))

;;;; force and collision


(defun init-rolling-rigids ()
  ;;
  (let*
      ((planet-1 (make-instance 'sphere
                                :mass 1.00e12
                                :radius 8.00
                                :position (vector-3d -15.00 0.00 0.00)
                                :rotation-velocity (rotation-quaternion-of-rodrigues
                                                    0.001 (vector-3d 1.00 0.10 0.00))))
       (planet-2 (make-instance 'sphere
                                :mass 1.50e12
                                :radius 4.00
                                :position (vector-3d 00.00 20.00 0.00)
                                :rotation-velocity (rotation-quaternion-of-rodrigues
                                                    0.008 (vector-3d 1.00 0.00 0.10))))
       (len-rigids 10)
       (balls
         (loop for i from 0 to (1- len-rigids)
               collect (make-instance 
                        'sphere
                        :mass 1.00e1
                        :radius 1.00
                        :position (vector-3d (random 10.00) (random 10.00) (random 10.00))
                        :velocity (vector-3d (random 10.00) (random 10.00) (random 10.00))
                        :rotation-velocity (rotation-quaternion-of-rodrigues
                                            0.01 (vector-3d 1.00 0.00 0.00))))))
    (append (list planet-1 planet-2) balls)))


;;;; universal gravitation

(defmethod inner-or-outer-mass ((rigid sphere) radius-from-point-to-gravity-center)
  (with-slots (mass (radius-of-rigid radius)) rigid
    (let ((rho (/ mass
                  (* 3/4 *pi* (expt radius-of-rigid 3)))))
    (cond 
      ((> radius-of-rigid radius-from-point-to-gravity-center)
       (* rho (* 3/4 *pi* radius-from-point-to-gravity-center))) ;; inner mass
      (t
       mass ;; outer mass
       )))))

(defparameter *gravitation-constant* 6.67430e-11) ;; [m^3*kg^-1*s^-2]

(defun universal-gravitation (rigid-a rigid-b)
  ;; for outer acceleration,
  ;; a_n = F_n/m_n = G\frac{m_1m_2}{|R^2| m_n}\dot{R} 
  ;;               = G\frac{m_1m_2}{|R^3|} \cdot \frac{R}{m_n}
  ;; for inner acceleration, referenced
  ;; [球対称な物体による万有引力 | 高校物理の備忘録](https://physnotes.jp/mechanics/shell_theorem/#i-8)
  (with-slots ((position-a position) (mass-a mass) (radius-a radius)) rigid-a
    (with-slots ((position-b position) (mass-b mass) (radius-b radius)) rigid-b
      (let* ((relative (vec3-sub position-a position-b))
             (dist (vec3-magnitude relative))
             (active-mass-a (inner-or-outer-mass rigid-a dist))
             (active-mass-b (inner-or-outer-mass rigid-b dist)))
        (let* ((abs-force (/ (* *gravitation-constant* active-mass-a active-mass-b)
                             (* (expt (vec3-magnitude relative) 3))))
               (accel-a (vec3-multiply (vec3-reverse relative) (/ abs-force mass-a)))
               (accel-b (vec3-multiply (identity relative) (/ abs-force mass-b))))
          (values accel-a accel-b))))))

(defun action-and-reaction-universal-gravitation! (rigid-a rigid-b)
  (multiple-value-bind (accel-to-a accel-to-b)
      (universal-gravitation rigid-a rigid-b)
    (let ((accel-a (slot-value rigid-a 'acceleration))
          (accel-b (slot-value rigid-b 'acceleration)))
      (setf (slot-value rigid-a 'acceleration)
            (vec3-add accel-a accel-to-a))
      (setf (slot-value rigid-b 'acceleration)
            (vec3-add accel-b accel-to-b))))
  (values rigid-a rigid-b))


;;;; collision-force 


(defun collisioning-point-and-time-of-moving-spheres (sphere-a sphere-b)
  ;; http://marupeke296.com/COL_3D_No9_GetSphereColliTimeAndPos.html
  sphere-a sphere-b
  nil
)

(defparameter *rebound-rate* 0.7)

(defun restitution (rigid-a rigid-b)
  ;; for sphere
  ;; http://marupeke296.com/COL_MV_No1_HowToCalcVelocity.html
  (when (collision-detection rigid-a rigid-b)
    (with-slots ((v-a velocity) (m-a mass) (p-a position) (radius-a radius)) rigid-a
      (with-slots ((v-b velocity) (m-b mass) (p-b position) (radius-b radius)) rigid-b
        (let* ((vector-center-to-center (vec3-sub p-b p-a)) ;; todo get collisioning position
               (time *time-step*) ;; todo get collisioning time
               ;;
               (total-mass (+ m-a m-b))
               (refrect-rate (+ 1 (* *rebound-rate* *rebound-rate*)))
               (axis-vector (vec3-normalize vector-center-to-center))
               (axis-dot (vec3-dot (vec3-sub v-a v-b) axis-vector))
               (const-vec (vec3-multiply axis-vector (* refrect-rate axis-dot (/ total-mass))))
               ;;
               )
          
          ;; veclocity after collision
          (setf v-a (vec3-add v-a (vec3-multiply const-vec (* -1 m-b))))
          (setf v-b (vec3-add v-b (vec3-multiply const-vec (* +1 m-a))))
          ;; position  after collision
          (setf p-a (vec3-add p-a (vec3-multiply v-a time)))
          (setf p-b (vec3-add p-b (vec3-multiply v-b time)))
          ;; push back
          ;;(setf p-a nil)

          
          ;;
          (values rigid-a rigid-b))))))


(defun action-and-reaction-rigid-sphere-collisioning! (rigid-a rigid-b)
  ;; only sphere
  ;; rigid collisioning has no mass-change and has no 
  ;; in Static(f1=f2) Physics
  (multiple-value-bind (coll-to-a coll-to-b)
      (collision-detection rigid-a rigid-b)
    (cond ((or coll-to-a coll-to-b) 
           (restitution rigid-a rigid-b))
          (t
           nil))))

;;;; update rigids

(defun update-rolling-rigids (rigids delta_time)
  rigids delta_time
  ;;; initialize for timestep
  (loop for rigid in rigids
        do (setf (slot-value rigid 'acceleration) *zero-vec3*))

  ;;; R(n, time+delta_time) = R(n, time) + delta * func(n, rigids)

  ;; l.1 universal gravitation
  (loop for i_a from 0 to (1- (length rigids))
        do (loop for i_b from 0 to (- i_a 1)
                 do (action-and-reaction-universal-gravitation! (nth i_a rigids)
                                                                (nth i_b rigids))))

  (loop for rigid in rigids
        do (setf (slot-value rigid 'velocity)
                 (vec3-add (slot-value rigid 'velocity)
                           (slot-value rigid 'acceleration))))

  ;; l.2 collision and momentum
  ;;(loop for i_a from 0 to (1- (length rigids))
  ;;do (loop for i_b from 0 to (- i_a 1)
  ;;do (action-and-reaction-rigid-sphere-collisioning! (nth i_a rigids)
  ;;(nth i_b rigids))))

  ;; k. 慣性移動、慣性回転



  ;; m. 衝突
  ;; m.1. 平行衝突
  ;; m.2. モーメント交換

  ;;; update all
  (loop for rigid in rigids
        do (update-object-physically rigid)))
        ;;do (with-slots (position velocity acceleration) rigid
  ;;(setf position
  ;;(vec3-add position (vec3-multiply velocity *time-step*))))))

(defun project-rolling-rigids (rigids)
  (mapcar #'(lambda (rigid)
              ;;(projection-wire rigid)
              (projection rigid)
              (project-rotaion-axis 
               (slot-value rigid 'rotation)))
          rigids)
  )




;;;; rolling ball model

(defclass rolling-ball-model (model)
  ((focussing-sphere :initform (make-instance 'sphere :radius 0.1))
   ;;
   (terrain :initform (funcall #'make-terrain))
   (rigids :initform (funcall #'init-rolling-rigids))
))

(defclass test-rolling-ball-window (simulator-window)
  ((model :initform (make-instance 'rolling-ball-model))
   )
  (:default-initargs :title "rolling ball window"))


(defmethod access-sphere ((w test-rolling-ball-window))
  ;;(aref (model-objects (displays-model w)) 0))
  (slot-value (displays-model w) 'focussing-sphere))

(defmethod glut:display-window :before ((w test-rolling-ball-window))
  (setf (slot-value (displays-camera w) 'focussing)
        (access-sphere w))

  (init-simulator-window w))

;;;;

(defparameter *delta-theta* (/ *pi* 96))

 
(defmethod update-displaying-by-input ((w test-rolling-ball-window))
  (let ((wrapper (input-wrapper w))
        (width (glut:get :window-width))
        (height (glut:get :window-height)))

    ;; camera
    (let ((camera (displays-camera w)))
      (with-slots (mouse-left mouse-middle mouse-xy-delta mouse-wheel) wrapper
        (if (eql mouse-wheel :up)         (fovy-zoom camera +1.00))
        (if (eql mouse-wheel :wheel-down) (fovy-zoom camera -1.00)))
      (manipulate-camera-wraparound-by-mouse-move camera wrapper width height 32 
                                                  'mouse-right 'mouse-middle)
      (manipulate-camera-wraparound-by-key camera wrapper 0.01 :roll
                                           #\j #\k ))
    ;; object sphere
    (let ((sph (access-sphere w)))
      (manipulate-obj-rotation-by-key sph wrapper *delta-theta*
                                      #\e #\z #\a #\d #\w #\s)
      (manipulate-obj-move-xyz-axis-by-key sph wrapper 0.02
                                           :key-left :key-right :key-down :key-up #\m #\n))
    ;; and more handling
    (with-slots (keys) wrapper
      ;; time step
      (with-slots (configured-time-step) (displays-timer w)
        (if (key-just-pressed? #\p keys)
            (switch-call-or-stop-timer (displays-timer w))) ;; pause and restart
        (if (key-pressing? #\o keys) (setf configured-time-step (* configured-time-step (/ 1 1.05))))
        (if (key-pressing? #\i keys) (setf configured-time-step (* configured-time-step (* 1 1.05))))
      ))))


(defmethod en-projector-from-model ((w test-rolling-ball-window))
  (with-slots (terrain rigids) (slot-value w 'model)
    ;;(project-terrain terrain)
    (project-rolling-rigids rigids))
        ;;
  (let ((obj-sph (access-sphere w)))
    (with-slots (displays-timer) w
      (with-slots (frame-counter) displays-timer
        (gl:with-pushed-matrix
          ;;
          (projection obj-sph)

          (gl:material :front-and-back :ambient-and-diffuse *color-mediumpurple*)
          ;(projection-slots-params obj-sph :xyz-axis t :rotation-axis t)

          (with-slots (position) obj-sph
            (project-vector position (object-forward obj-sph) :color *color-red*)
            (project-vector position (object-up obj-sph) :color *color-green*)
            (project-vector position (object-right obj-sph) :color *color-blue*))

          (project-xyz-axis)
          ;;
          )))))




(defmethod draw-status-texts ((w test-rolling-ball-window))
  w
  (gl-color *color-white*)
  #|
  ;; check screen (2D) position
  (loop for i from 0 to 10
        do (loop for j from 0 to 10
                 do (draw-string-2d (format nil "(~A,~A)" i j)
                                    i j 20 20)))
  |#
  ;(draw-string-2d (format-timer-template-status (displays-timer w)) 0.7 0.9)
  ;(draw-string-2d (format-input-status (input-wrapper w)) 0.7 0.5)
  ;(draw-string-2d (format-camera-status (displays-camera w)) 0.02 0.9)
  ;(draw-string-2d (describe-object-to-string (access-sphere w)) 0.02 0.5)
  ;;
  (let ((rigids (slot-value (displays-model w) 'rigids)))
      (draw-string-2d
       (format nil "rigid: ~A~%" (describe-object-to-string (nth 0 rigids)))
       0.1 0.9))
    )


;;;;


(defun update-world-by-input (objects input)
  objects input
  )


(defmethod update-model ((m rolling-ball-model))
  ;; update worlds
  (with-slots (rigids) m
    (update-rolling-rigids rigids *default-time-step*))
  ;;
  ;; new = lim (d_t->0) (objects(t+d_t) - objects(t) / dt
)


;;

(defun test-of-rolling-ball ()
  (let ((window (make-instance 'test-rolling-ball-window)))
    ;; timer
    (timer-s-params-set window 0)
    (call-timer window 0)
    ;; key
    (glut:set-key-repeat :key-repeat-off)
    ;;
    (glut:display-window window)
    ))

