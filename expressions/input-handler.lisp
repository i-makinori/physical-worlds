
(in-package :physical-worlds)

;; input handler

;; :no-modifier :with-shift :with-ctrl :with-alt
;; nil :pressing :pressed :released

(defclass input-wrapper ()
  ;; wrapper for inputting device
  ((keys
    :initform (make-hash-table))
   ;;
   (mouse-xy-start
    :initform nil)
   (mouse-xy-delta
    :initform nil)
   (mouse-xy-current
    :initform nil)
   ;;
   (mouse-left
    :initform nil)
   (mouse-middle
    :initform nil)
   (mouse-right
    :initform nil)
   (mouse-wheel
    :initform nil))
  )

(defun key-pressing? (key keys)
  (let ((pressing-key (car (gethash key keys))))
    (or (eql pressing-key :pressing) (eql pressing-key :just-pressed))))

(defun key-just-pressed? (key keys)
  (eql (car (gethash key keys)) :just-pressed))

(defmethod reset-for-frame-update---input-wrapper-status ((wrapper input-wrapper))
  (with-slots (keys mouse-xy-delta mouse-wheel) wrapper
    ;;
    (maphash #'(lambda (key value)
                 (setf (gethash key keys)
                       (cons :pressing (cdr value))))
             keys)
    ;;
    (setf mouse-xy-delta (2d-point 0 0))
    (setf mouse-wheel nil)))


(defmethod format-input-status ((wrapper input-wrapper))
  (with-slots (
               mouse-xy-start mouse-xy-delta mouse-xy-current
               mouse-left mouse-middle mouse-right
               keys) 
      wrapper
    (let
        ((mouse-coord-text
           (format nil "Mouse: start:~A, delta:~A, current:~A"
                   mouse-xy-start mouse-xy-delta mouse-xy-current))
         (mouse-pressing-text
           (format nil "Left:~A, Middle:~A, Right:~A"
                   mouse-left mouse-middle mouse-right))
         (keys-text
           (format nil "size:~A,~%keys:~A"
                   (hash-table-count keys)
                   (loop for k being the hash-key using (hash-value v) of keys 
                         collect (format nil "(~A: ~A)" k v)))))
      (format nil "~A~%~A~%~A~%" mouse-coord-text mouse-pressing-text keys-text))))


;;;; and manipulate unitility


(defmethod manipulate-obj-move-xyz-axis-by-key 
    ((obj domain-object) (iw input-wrapper) d_move
     key_x- key_x+ key_y- key_y+ key_z- key_z+)
  (with-slots (keys) iw
  (move-object! obj
                (vector-3d 
                 (+ (if (key-pressing? key_x- keys) (- d_move) 0)
                    (if (key-pressing? key_x+ keys) (+ d_move) 0))
                 (+ (if (key-pressing? key_y- keys) (- d_move) 0) 
                    (if (key-pressing? key_y+ keys) (+ d_move) 0))
                 (+ (if (key-pressing? key_z- keys) (- d_move) 0) 
                    (if (key-pressing? key_z+ keys) (+ d_move) 0))))))

(defmethod manipulate-obj-move-relate-camera-by-key
    ((obj domain-object) (iw input-wrapper) d_move (cam euler-camera)
     key_forward- key_forward+ key_up- key_up+ key_right- key_right+)
  (with-slots (keys) iw
    (with-slots (velocity) obj
      ;(with-slots () cam
      (let* ((camera-forward (vec3-normalize (forward cam)))
             (camera-up (vec3-normalize (looks-up cam)))
             (camera-right (vec3-normalize (vec3-cross camera-forward camera-up)))
             (component-list
               (list
                (if (key-pressing? key_forward- keys)
                    (vec3-reverse camera-forward) *zero-vec3*)
                (if (key-pressing? key_forward+ keys)
                    (identity camera-forward) *zero-vec3*)
                (if (key-pressing? key_up- keys)
                    (vec3-reverse camera-up) *zero-vec3*)
                (if (key-pressing? key_up+ keys)
                    (identity camera-up) *zero-vec3*)
                (if (key-pressing? key_right- keys)
                    (vec3-reverse camera-right) *zero-vec3*)
                (if (key-pressing? key_right+ keys)
                    (identity camera-right) *zero-vec3*)))
             (v_d (reduce #'vec3-add component-list))
             (v (if (vec3-zerovecp v_d) *zero-vec3* 
                    (vec3-multiply (vec3-normalize v_d) d_move))))
        (setf velocity v)
        (update-object obj) ;; !!
        ;; (move-object obj v)
        ))))

(defmethod manipulate-obj-rotation-by-key
    ((obj domain-object) (iw input-wrapper) d_theta
     key-roll- key-roll+ key-pitch- key-pitch+ key-yaw- key-yaw+)
  (with-slots (keys) iw
    (if (key-pressing? key-roll- keys) (enroll obj (- d_theta))) ;; roll
    (if (key-pressing? key-roll+ keys) (enroll obj (+ d_theta)))
    (if (key-pressing? key-pitch- keys) (enpitch obj (- d_theta))) ;; pitch
    (if (key-pressing? key-pitch+ keys) (enpitch obj (+ d_theta)))
    (if (key-pressing? key-yaw- keys) (enyaw obj (- d_theta))) ;; yaw
    (if (key-pressing? key-yaw+ keys) (enyaw obj (+ d_theta)))))


(defmethod manipulate-camera-wraparound-by-key
    ((c euler-camera) (iw input-wrapper) d_theta or-roll_pitch_yaw
     key+ key-)
  (with-slots (keys) iw
    (let ((function
            (case or-roll_pitch_yaw
              (:roll #'roll-camera)
              (:pitch #'pitch-camera)
              (:yaw #'yaw-camera)
              (otherwise #'identity))))
      (if (key-pressing? key+ keys) (funcall function c (+ d_theta)))
      (if (key-pressing? key- keys) (funcall function c (- d_theta))))))


(defmethod manipulate-camera-wraparound-by-mouse-move
    ((cam euler-camera) (iw input-wrapper) width height wrap-const
     button-roll button-pitch-and-yaw)
  (with-slots (mouse-xy-delta mouse-wheel) iw
    (let ((fovy-variable
            (* 2 pi 1/6))) ; if const ; 1/6 means 1/6 rotation here.
            ;;(degree-to-radian (slot-value cam 'theta-fovy)))) ; if rotation relates to fovy.
      ;; roll
      (when (and (slot-value iw button-roll)
                 (2d-pointp mouse-xy-delta))
        (roll-camera  cam (* fovy-variable wrap-const (safe-div (2d-point-y mouse-xy-delta) height))))
      ;; pitch yaw
      (when (and (slot-value iw button-pitch-and-yaw)
                 (2d-pointp mouse-xy-delta))
        (pitch-camera cam (* fovy-variable wrap-const (safe-div (2d-point-x mouse-xy-delta) width)))
        (yaw-camera   cam (* fovy-variable wrap-const (safe-div (2d-point-y mouse-xy-delta) height))))
      )))



