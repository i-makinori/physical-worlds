
(in-package :physical-worlds)

;; list of symbol names
(defun list-fboundp-symbols-of-package (package-name)
  (let ((pack (find-package package-name))
        (symbols '())
        (stream (make-string-output-stream)))
    stream
    (do-all-symbols (sym pack)
      (when (eql (symbol-package sym) pack)
        ;(setf symbols (cons (print sym) symbols)))
        (setf symbols (cons (print sym stream) symbols))))
    symbols))

;; export symbols


(defun export-physical-worlds-symbols ()
  (let ((symbols (list-fboundp-symbols-of-package :physical-worlds)))
    (mapcar #'(lambda (symbol)                
                (export (list symbol)
                        :physical-worlds))
            symbols)))

(export-physical-worlds-symbols)

