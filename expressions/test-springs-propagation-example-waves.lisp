(in-package :physical-worlds)

;;;; force initial examples
(defun _whole-objects-initial-1_ ()
  (let* ((point-masses
           (list
            (init-static-object  1.00 #1A(1 0 0)) ; 0
            (init-dynamic-object 1.00 #1A(2 0 0) #1A(0 0 0) #1A(0 0 0)) ; 1
            (init-dynamic-object 1.00 #1A(3 0 0) #1A(0 0 0) #1A(0 0 0)) ; 2
            (init-static-object  1.00 #1A(4 0 0)) ; 3
            ))
         (ps point-masses))
    (append
     point-masses ;; pointmasses
     (list ;; springs
      (init-spring-object 0.5 80 (nth 0 ps) (nth 1 ps))
      (init-spring-object 0.5 40 (nth 1 ps) (nth 2 ps))
      (init-spring-object 0.5 80 (nth 2 ps) (nth 3 ps))))))

(defun _whole-objects-initial-2_ ()
  (let* ((n-dynamics 5)
         (dynamic-masses
           (loop for pos-i from (+ 1 1) to (+ (+ 1 1) n-dynamics -1)
                 collect (init-dynamic-object 0.10 (vector-3d pos-i 0 0) #1A(0 0 0) #1A(0 0 0))))
         (point-masses
           (append `(,(init-static-object 1.00 (vector-3d 1 0 0))) ;; 0
                   dynamic-masses ;; 1 to n-dynamics
                   `(,(init-static-object 1.00 (vector-3d (+ n-dynamics 1 1) 0 0))))) ;; 1+ n-dynamics
         (springs
           (loop for i from 0 to (+ 0 n-dynamics)
                 collect (init-spring-object 0.80 80 (nth i point-masses) (nth (1+ i) point-masses)
                                             :mass 1.00))))
    ;; forced vibrations position
    (setf (slot-value (nth 0 dynamic-masses) 'forced-position)
          #'(lambda (obj time) obj (vector-3d 2 0 (* 1 (sin (* time 2 *pi*))))))
    ;#'(lambda (obj time) obj time (vector-3d 2 0 2)))

    ;; forced vibrations velocity
    ;(setf (slot-value (nth 0 dynamic-masses) 'forced-velocity)
          ;#'(lambda (obj time) obj time (vector-3d 0 (* 1 (sin (* time 2 *pi*))) 0)))
          ;#'(lambda (obj time) obj time (vector-3d 0 9 0)))
          ;#'(lambda (obj time) obj (if (zerop time) (vector-3d 0 0 10) (vector-3d 0 0 0))))
    
    ;; forced vibrations acceleration
    ;(setf (slot-value (nth 0 dynamic-masses) 'forced-acceleration)
          ;#'(lambda (obj time) obj (vector-3d 0 (* 20 (sin (* time 0.5 *pi*))) 0)))
          ;#'(lambda (obj time) obj time (vector-3d 0 0 8)))

    ;; append
    (coerce (append point-masses springs) 'vector)))

(defun _whole-objects-initial-nawatobi_ ()
  (let* ((n-dynamics 50)
         (radius-const 50)
         (gap 0.1)
         (dynamic-masses
           (loop for pos-i from (+ 1 1) to (+ (+ 1 1) n-dynamics -1)
                 collect (init-dynamic-object 
                          0.8  (vector-3d (* pos-i gap) 0 0) #1A(0 0 0)  #1A(0 0 0))))
         (point-masses
           (append
            `(,(init-static-object 1.00 (vector-3d (* 1 gap) 0 0))) ;; 0
            dynamic-masses ;; 1 to n-dynamics
            `(,(init-static-object 1.00 (vector-3d (* (+ n-dynamics 1 1) gap) 0 0))))) ;; 1+ n-dynamics
         (springs
           (loop for i from 0 to (+ 0 n-dynamics)
                 collect (init-spring-object 
                          (* gap 0.5) 800 (nth i point-masses) (nth (1+ i) point-masses)
                          :mass 1.00))))
    (setf (slot-value (nth 0 dynamic-masses) 'forced-acceleration)
          #'(lambda (obj time) obj (vector-3d (* 0)
                                              (* radius-const (cos (* time 2/3 *pi*)))
                                              (* radius-const (sin (* time 2/3 *pi*))))))
    (setf (slot-value (nth (1- n-dynamics) dynamic-masses) 'forced-acceleration)
          #'(lambda (obj time) obj (vector-3d (* 0)
                                              (* radius-const (cos (* time 2/3 *pi*)))
                                              (* radius-const (sin (* time 2/3 *pi*))))))
    (coerce (append point-masses springs) 'vector)))

(defun _whole-objects-initial-delta-star-0_ ()
  (let*
      (;
       ; 1 1 2 3 5 8 13 21 34 ...
       (n-points 13) 
       (radius 3)
       (amplitude 1.0)
       ;(zeroth-phase 0)
       (first-phase (* 1 *pi* (/ n-points))) ;; 1 * (2*pi) * (1/2*1/n) = 
       (second-phase (* 2 *pi* (/ n-points))) ;; 2 * ...
       (third-phase (* 2 *pi* (/ n-points))) ;; 2 * ...
       ;; point masses
       (zeroth-dynamics ;; first point
         (list (init-dynamic-object 1.00 *zero-vec3* *zero-vec3* *zero-vec3*)))
       (first-dynamics ;; first-circle
         (loop for i from 0 to (1- n-points)
               collect (init-dynamic-object 
                        1.00 
                        (vector-3d (* radius 1/2 (cos (+ (* 2 *pi* i (/ n-points)) first-phase)))
                                   0
                                   (* radius 1/2 (sin (+ (* 2 *pi* i (/ n-points)) first-phase))))
                        *zero-vec3* *zero-vec3*)))
       (second-dynamics ;; second-circle
         (loop for i from 0 to (1- n-points)
               collect (init-dynamic-object
                        1.00
                        (vector-3d (* radius 1/1 (cos (+ (* 2 *pi* i (/ n-points)) second-phase)))
                                   0
                                   (* radius 1/1 (sin (+ (* 2 *pi* i (/ n-points)) second-phase))))
                        *zero-vec3* *zero-vec3*)))
       (third-statics ;; third-circle
         (loop for i from 0 to (1- n-points)
               collect (init-static-object
                        1.00
                        (vector-3d (* radius 3/2 (cos (+ (* 2 *pi* i (/ n-points)) third-phase)))
                                   0
                                   (* radius 3/2 (sin (+ (* 2 *pi* i (/ n-points)) third-phase))))
                        ;*zero-vec3* *zero-vec3*
                        )))
       ;; springs
       (springs-zero-and-firsts
         (loop for i from 0 to (1- n-points)
               collect (init-spring-object 1.00 100
                                           (nth 0 zeroth-dynamics)
                                           (nth i first-dynamics))))
       (springs-firsts-neighbours
         (loop for i from 0 to (1- n-points)
               collect (init-spring-object 1.00 100
                                           (nth i first-dynamics)
                                           (nth (mod (1+ i) n-points) first-dynamics))))
       (springs-firsts-and-seconds
         (loop for i from 0 to (1- n-points)
               collect (init-spring-object 1.00 100
                                           (nth i first-dynamics)
                                           (nth i second-dynamics))
               collect (init-spring-object 1.00 100
                                           (nth (mod (1+ i) n-points) first-dynamics)
                                           (nth i second-dynamics))))
       (springs-seconds-neighbours
         (loop for i from 0 to (1- n-points)
               collect (init-spring-object 1.00 100
                                           (nth i second-dynamics)
                                           (nth (mod (1+ i) n-points) second-dynamics))))
       (springs-seconds-and-thirds
         (loop for i from 0 to (1- n-points)
               collect (init-spring-object 1.00 100
                                           (nth i second-dynamics)
                                           (nth i third-statics))
               collect (init-spring-object 1.00 100
                                           (nth (mod (1+ i) n-points) second-dynamics)
                                           (nth i third-statics))))
       )


    ;; forced vibration
    (setf (slot-value (nth 0 zeroth-dynamics) 'forced-position)
          #'(lambda (obj time) obj time
              (vector-3d (* amplitude (sin (* time 1/2 *pi*)))
                         (* amplitude (sin (* time 2/1 *pi*)))
                         (* amplitude (cos (* time 1/2 *pi*)))
                         
                         )))
    ;;
          
    ;; append and return
    (coerce
     (append
      ;
      zeroth-dynamics
      first-dynamics
      second-dynamics
      third-statics
      ;
      springs-zero-and-firsts
      springs-firsts-neighbours
      springs-firsts-and-seconds
      springs-seconds-neighbours
      springs-seconds-and-thirds
      )
    'vector

)))

(defun _whole-objects-initial-matrix-0_ ()
  (let*
      ((n-row 9)
       (n-col 9)
       (gap 0.4)
       (radius-const 0.5)
       ;; point masses
       (dynamic-masses-2d
                (loop for row from 0 to (1+ n-row)
                      collect (loop for col from 0 to (1+ n-col)
                                    collect 
                                    (if (or (= row 0) (= row (1+ n-row))
                                            (= col 0) (= col (1+ n-col)))
                                        (init-static-object 1.00
                                                            (vector-3d (* row gap) 0 (* col gap)))
                                        (init-dynamic-object 1.00
                                                             (vector-3d (* row gap) 0 (* col gap))
                                                             #1A(0 0 0)
                                                             #1A(0 0 0))))))
       ;; springs
       (row-springses
         (loop for row from 0 to n-row
               collect (loop for col from 0 to (1+ n-col)
                        collect (init-spring-object (* gap 1.00) 100
                                                    (nth col (nth (+ row 0) dynamic-masses-2d))
                                                    (nth col (nth (+ row 1) dynamic-masses-2d))))))
       (col-springses
         (loop for row from 0 to (1+ n-row)
               collect (loop for col from 0 to n-col
                        collect (init-spring-object (* gap 1.00) 100 
                                                    (nth (+ col 0) (nth row dynamic-masses-2d))
                                                    (nth (+ col 1) (nth row dynamic-masses-2d)))))))
    ;; force
    (setf (slot-value (nth 5 (nth 5 dynamic-masses-2d)) 'forced-position)
          #'(lambda (obj time) obj 
              (with-slots (position) obj
                (vector-3d 
                 (vec3-s1 position)
                 (* radius-const (sin (* time 1.5 *pi*)))
                 (vec3-s3 position)))))
    ;; array
    (concatenate
     'vector
     (apply #'append dynamic-masses-2d)
     (apply #'append row-springses)
     (apply #'append col-springses))))



(defun _whole-objects-initial-matrix-oosakajime_ ()
  (let*
      ((n-row 9) 
       (n-col 9)
       (gap 0.4)
       (radius-const 0.5)
       (spring-constant 1000) (natural-length gap)
       ;; point masses
       (dynamic-masses-2d
                (loop for row from 0 to (1+ n-row)
                      collect (loop for col from 0 to (1+ n-col)
                                    collect 
                                    (if (or (= row 0) (= row (1+ n-row))
                                            (= col 0) (= col (1+ n-col)))
                                        (init-static-object 1.00
                                                            (vector-3d (* row gap) 0 (* col gap)))
                                        (init-dynamic-object 1.00
                                                             (vector-3d (* row gap) 0 (* col gap))
                                                             #1A(0 0 0)
                                                             #1A(0 0 0))))))
       ;; springs
       (row-springses
         (loop for row from 0 to n-row
               collect (loop for col from 0 to (1+ n-col)
                        collect (init-spring-object natural-length spring-constant
                                                    (nth col (nth (+ row 0) dynamic-masses-2d))
                                                    (nth col (nth (+ row 1) dynamic-masses-2d))))))
       (col-springses
         (loop for row from 0 to (1+ n-row)
               collect (loop for col from 0 to n-col
                        collect (init-spring-object natural-length spring-constant
                                                    (nth (+ col 0) (nth row dynamic-masses-2d))
                                                    (nth (+ col 1) (nth row dynamic-masses-2d)))))))
    ;; force
    (setf (slot-value (nth 5 (nth 5 dynamic-masses-2d)) 'forced-position)
          #'(lambda (obj time) obj
              (with-slots (position) obj
                (cond 
                  ((or 
                    ;; Todo : implement true timing for oosakajime pulse.
                    ;; especially, timing ratio of final three(?) claps.
                    (< 10.00 time 10.10) (< 10.50 time 10.60)
                    (< 12.00 time 12.10) (< 12.50 time 12.60)
                    (< 14.00 time 14.10) (< 14.16 time 14.26) (< 14.50 time 14.60)) ;; 1:1:2 => 1:2 ?
                   (vector-3d 
                    (vec3-s1 position)
                    radius-const
                    (vec3-s3 position)))
                  (t
                   (vector-3d 
                    (vec3-s1 position)
                    0
                    (vec3-s3 position)))))))
    ;; array
    (concatenate
     'vector
     (apply #'append dynamic-masses-2d)
     (apply #'append row-springses)
     (apply #'append col-springses))))
