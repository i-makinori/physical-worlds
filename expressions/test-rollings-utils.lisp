
(in-package :physical-worlds)

;;;; icosahedron

(defparameter *phi* (/ (+ 1 (sqrt 5)) 2 ))

(defun icosahedron-vertex-index-list ()
  ;; icosahedron ;; simetric 20 planes solid
  (labels
      ((cyc12 (n g d) (mod (+ n (* g 4) d) 12)) ; cycle group. diff, group, num
       (gen-clockwise-symmetric-list (n)
         (list (list (+ n 0) (+ n 0 +2) (cyc12 n 2 0) (cyc12 n 1 0) (cyc12 n 1 2) (cyc12 n 2 1))
               (list (+ n 1) (+ n 1 +2) (cyc12 n 2 3) (cyc12 n 1 2) (cyc12 n 1 0) (cyc12 n 2 2))
               (list (+ n 2) (+ n 2 -2) (cyc12 n 2 1) (cyc12 n 1 3) (cyc12 n 1 1) (cyc12 n 2 0))
               (list (+ n 3) (+ n 3 -2) (cyc12 n 2 2) (cyc12 n 1 1) (cyc12 n 1 3) (cyc12 n 2 3))))
       (gen-triangles (center neighbours)
         (let ((n-len (length neighbours)))
           (loop for i from 0 to (1- n-len)
                 collect (list center (nth i neighbours) (nth (mod (+ i 1) n-len) neighbours))))))
    (let* (
           ;(phi 2)
           (epsilon `((0 ,(+ 1) ,(+ *phi*))
                      (0 ,(+ 1) ,(- *phi*))                    
                      (0 ,(- 1) ,(+ *phi*))
                      (0 ,(- 1) ,(- *phi*))))
           (permutations '((0 1 2) (1 2 0) (2 0 1)))
           (vertex-list
             (apply #'append (mapcar #'(lambda (p)
                                         ;; 4 * 6 vertexs
                                         (mapcar #'(lambda (n)
                                                     (vector-3d (nth (nth 0 p) (nth n epsilon))
                                                                (nth (nth 1 p) (nth n epsilon))
                                                                (nth (nth 2 p) (nth n epsilon))))
                                                 '(0 1 2 3)))
                                     permutations)))
           (n-neighbor-n-s
             (apply #'append (mapcar #'gen-clockwise-symmetric-list '(0 4 8))))
           (vertex-index-triangle-list
             (remove-duplicates
              (apply #'append
                     (mapcar #'(lambda (n-ns) (gen-triangles (car n-ns) (cdr n-ns)))
                             n-neighbor-n-s))
              :test #'equal-set-p :from-end t)))
      ;; for VAO or VBO implementation
      (values vertex-list vertex-index-triangle-list))))



;;;; vertex and vertexes-triangle-idnex-list

(defun whole-3split-triangles (vertex-list vertex-index-triangle-list &key (radius nil))
  (let* ((additional-index-list-reverse '())
         (additional-vertex-list-reverse))
    (loop
      for i from 0 to (1- (length vertex-index-triangle-list))
      collect
         (let* ((i_s_current (nth i vertex-index-triangle-list))
                ; i_(a,b,c), v_(a,b,c), index of this triangle's center (O).
                (i_a (nth 0 i_s_current)) (i_b (nth 1 i_s_current)) (i_c (nth 2 i_s_current))
                (v_a (nth i_a vertex-list)) (v_b (nth i_b vertex-list)) (v_c (nth i_c vertex-list))
                (index_this (+ i (length vertex-list)))
                ;
                (v_o_radius 
                  (if (numberp radius)
                      radius
                      (/ (+ (vec3-magnitude v_a) (vec3-magnitude v_b) (vec3-magnitude v_c)) 3)))
                (v_o_normal 
                  (vec3-normalize (vec3-division (vec3-add v_a (vec3-add v_b v_c)) 3)))
                (v_o (vec3-multiply v_o_normal v_o_radius)))
           (setf additional-vertex-list-reverse (cons v_o additional-vertex-list-reverse))
           (setf additional-index-list-reverse
                 (append
                  `((,index_this ,i_a ,i_b) (,index_this ,i_b ,i_c) (,index_this ,i_c ,i_a))
                  additional-index-list-reverse))))
    (values 
     (append vertex-list (reverse additional-vertex-list-reverse))
     (append '() (reverse additional-index-list-reverse)))))

#|
(multiple-value-bind (v i) (icosahedron-vertex-index-list)
  (whole-3split-triangles v i))
|#


(defun vertex-and-triangle-indexes-to-triangle-list (vertexs-list triangle-indexes-list)
  (mapcar #'(lambda (indexes)
              (let ((triangle
                      (make-instance 'triangle )))
                (with-slots (position position2 position3) triangle
                  (setf position  (nth (nth 0 indexes) vertexs-list))
                  (setf position2 (nth (nth 1 indexes) vertexs-list))
                  (setf position3 (nth (nth 2 indexes) vertexs-list))
                  (triangle-normal! triangle)
                  triangle)))
          triangle-indexes-list))

