
(in-package :physical-worlds)

;;;; algebra

;; solve by coefficient

(defun solve-linear-coefficient (a b)
  ;; solve x of a*x + b = 0
  (cond ((= a 0) 
         (if (= b 0) 
             0 ;; any of number is true answer.
             ; (error (format nil "algebra is wrong: ~A * x + ~A = 0" a b))
             nil))
        (t
         (/ (- b) a))))

(defun solve-quadratic-coefficient (a b c)
  ;; solve x of a*x^2 + b*x + c = 0
  ;; return list
  (let ((discrim (- (* b b) (* 4 a c))))
    (cond ((= a 0)
           (let ((linear (solve-linear-coefficient b c)))
             (if (null linear)
                 nil (list linear))))
          ((= discrim 0)
           (list (/ (- b) (* 2 a))))
          (t 
           (list (/ (+ (- b) (sqrt discrim)) (* 2 a))
                 (/ (- (- b) (sqrt discrim)) (* 2 a)))))))


;; calcuation utils

(defun square (num)
  (* num num))

(defparameter *pi* 3.1415926535897932384626433)

(defun radian-to-degree (radian)
  (* radian (/ 180 *pi*)))

(defun degree-to-radian (degree)
  (* degree (/ *pi* 180)))

(defparameter *scalar-unit-type* 'single-float)

(defun en-scalar-unit (number)
  (coerce number *scalar-unit-type*))

;; error


(defparameter +rounding-error-constant+ (expt 0.1 4)) ;; 100 micro meter. error for mathematical calcs
(defparameter +physical-error-constant+ (expt 0.1 2)) ;; 10 milli meter. error for physical simulation

(defun ~= (real1 real2)
  ;; almost equal(~=) number
  (> +rounding-error-constant+ (abs (- real1 real2))))

(defun a= (real1 real2)
  (> +physical-error-constant+ (abs (- real1 real2))))


;;;; linear algebra



;; >> (typep (vector-3d 1.0 2.0 3.0) 'vector-3d)
;; t

(defun vector-3d (s1 s2 s3)
  ;; vector-3dd ::= V{s1, s2, s3}
  (make-array (list 3) ;;:element-type *scalar-unit-type*
              :initial-contents (list (en-scalar-unit s1) (en-scalar-unit s2) (en-scalar-unit s3))))

(defun vec3-p (vec3?)
  (and (arrayp vec3?)
       (= (array-rank vec3?) 1)
       (= (array-dimension vec3? 0) 3)))

(defparameter *zero-vec3* (vector-3d 0.00 0.00 0.00))
(defparameter *base-vec3-axis-x* (vector-3d 1.00 0.00 0.00))
(defparameter *base-vec3-axis-y* (vector-3d 0.00 1.00 0.00))
(defparameter *base-vec3-axis-z* (vector-3d 0.00 0.00 1.00))
;; (defparameter *base-vec3-identity* (vector-3d 1.00 1.00 1.00))

(defparameter *sample-vec3-a* (vector-3d 3 4 -5))
(defparameter *sample-vec3-b* (vector-3d 0.5 2 -1))

(defun vec3-s1 (vec3)
  ;; vec3[s1]
  (aref vec3 0))

(defun vec3-s2 (vec3)
  ;; vec3[s2]
  (aref vec3 1))

(defun vec3-s3 (vec3)
  ;; vec3[s3]
  (aref vec3 2))

(defun mapf-to-vec3 (function vec3-a vec3-b &optional (const 0.00))
  ;; V{f({a.s1, b.s1, const}), f({a.s2, b.s2, const}), f({a.s3, b.s3, const})}
  (vector-3d (funcall function (vec3-s1 vec3-a) (vec3-s1 vec3-b) const)
             (funcall function (vec3-s2 vec3-a) (vec3-s2 vec3-b) const)
             (funcall function (vec3-s3 vec3-a) (vec3-s3 vec3-b) const)))


(defun vec3-add (vec3-a vec3-b)
  ;; vec3-a + vec3-b
  (mapf-to-vec3 #'+ vec3-a vec3-b 0.00))

(defun vec3-sub (vec3-a vec3-b)
  ;; vec3-a - vec3-b
  (mapf-to-vec3 #'- vec3-a vec3-b 0.00))

(defun vec3-reverse (vec3)
  ;; - vec3
  (vec3-sub *zero-vec3* vec3))

(defun vec3-multiply (vec3 scalar)
  ;; vec3 * scalar
  (mapf-to-vec3 #'* vec3 (vector-3d 1.00 1.00 1.00) scalar))

(defun vec3-division (vec3 scalar)
  ;; vec3 / scalar
  (mapf-to-vec3 #'/ vec3 (vector-3d 1.00 1.00 1.00) scalar))

(defun vec3-dot (vec3-a vec3-b)
  ;; sigma(n={s1,s2,s3}, a[n]*b[n])
  (reduce #'+ (mapf-to-vec3 #'* vec3-a vec3-b 1.00)))

(defun vec3-magnitude^2 (vec3)
  ;; || vec3 || ^ 2
  ;; l^2 = sigma(n={s1,s2,s3}, a[n]*a[n]) = dot(a,a)
  (vec3-dot vec3 vec3))

(defun vec3-magnitude (vec3)
  ;; || vec3 ||
  (sqrt (vec3-magnitude^2 vec3)))

(defun vec3-zerovecp (vec3)
  (zerop (vec3-magnitude^2 vec3)))

(defun vec3-normalize (vec3)
  ;; vec3 / || vec3 ||
  (vec3-division vec3 (vec3-magnitude vec3)))

(defun vec3-normalizedp (vec3)
  ;; || vec3 || ?= 1
  ;; 1^2 = 1
  (~= (vec3-magnitude^2 vec3) 1))

(defun vec3-vec3-abs-dot-angle (vec3-a vec3-b)
  ;; dot(a, b)  = || a || || b || cos(theta) =>
  ;; cos(theta) = dot(vec3-a, vec3-b) / (|| vec3-a || || vec3-b ||)
  ;; [radian]
  (acos (/ (vec3-dot vec3-a vec3-b) 
           (* (vec3-magnitude vec3-a) (vec3-magnitude vec3-b)))))

(defun vec3-cross (vec3-a vec3-b)
  ;; vec3-a (cross-product) vec3-b
  (vector-3d (- (* (vec3-s2 vec3-a) (vec3-s3 vec3-b)) (* (vec3-s3 vec3-a) (vec3-s2 vec3-b)))
             (- (* (vec3-s3 vec3-a) (vec3-s1 vec3-b)) (* (vec3-s1 vec3-a) (vec3-s3 vec3-b)))
             (- (* (vec3-s1 vec3-a) (vec3-s2 vec3-b)) (* (vec3-s2 vec3-a) (vec3-s1 vec3-b)))))


;;;; matrix

(defun vec3-to-vec4 (vec3 &optional (s4 1.00))
  (make-array (list 4)
              :initial-contents (list (vec3-s1 vec3) (vec3-s2 vec3) (vec3-s3 vec3)
                                      (en-scalar-unit s4))))
(defun vec4-sss0-vec3 (vec4)
  (vector-3d (aref vec4 0) (aref vec4 1) (aref vec4 2)))

(defparameter *sample-matrix-a*
  #2A((1 3 0 -2)
      (3 4 5 2)
      (3 2 1 1)))

(defparameter *sample-matrix-b*
  #2A((0.5 0.2)
      (-0.2 0.4)
      (-0.2 0.3)
      (0.1 0.5)))

(defparameter *sample-matrix-c*
  #2A((1 2 3)
      (4 5 6)
      (7 8 9)
      (3 6 9)))

(defun matrix-transpose (matrix)
  ;; transposed Matrix
  (let* ((dimensions (array-dimensions matrix))
         (new-mat (make-array (reverse dimensions))))
    (cond ((not (= 2 (array-rank matrix)))
           (error "array rank is not 2"))
          (t
           (loop for i from 0 to (1- (nth 0 dimensions))
                 do (loop for j from 0 to (1- (nth 1 dimensions))
                          do ;; (format t "i:~d, j:~d ~%" i j)
                             (setf (aref new-mat j i) 
                                   (aref matrix i j))))))
                 new-mat))

#|
(defmacro def-NxM-MxL-matrix-mulipcation (n m l)
  (defun 
  )
|#

(defun matrix-vector-multiplication (mat-a vec-b)
  ;; C = A b
  ;;
  (let ((dim-a (array-dimensions mat-a))
        (dim-b (array-dimensions vec-b)))
    (cond ((not (= (nth 1 dim-a) (nth 0 dim-b)))
           (error "dimension of matrix or vector is wrong."))
          (t
           (let ((new-vec (make-array (list (nth 0 dim-a)))))
             (loop 
               for i from 0 to (1- (nth 0 dim-a))
               do (setf 
                   (aref new-vec i)
                   (reduce #'+
                           (loop for j from 0 to (1- (nth 0 dim-b))
                                 collect (* (aref mat-a i j) (aref vec-b j))))))
             new-vec)))))

(defun matrix-multiplication (mat-a mat-b)
  ;; A.dim: lxm, B.dim: mxn
  ;; C = A B
  ;; C_ij = (sigma (k=1 to m) (A_ik * B_kj))
  ;; i=1 to l, j=1 to n
  (let ((dim-a (array-dimensions mat-a))
        (dim-b (array-dimensions mat-b)))
    (cond ((not (and (= 2 (array-rank mat-a)) (= 2 (array-rank mat-b))))
           (error "array rank is not 2"))
          ((not (= (nth 1 dim-a) (nth 0 dim-b)))
           (error "array dimension wrong"))
          (t
           (let ((new-mat (make-array (list (nth 0 dim-a) (nth 1 dim-b)))))
             (loop
               for i from 0 to (1- (nth 0 dim-a))
               do (loop
                    for j from 0 to (1- (nth 1 dim-b))
                    do (setf 
                        (aref new-mat i j)
                        (reduce #'+
                                (loop
                                  for k from 0 to (1- (nth 1 dim-a))
                                  collect (* (aref mat-a i k) (aref mat-b k j)))))))
             new-mat)))))

(defun matrix-multiplications (&rest matrixes)
  (reduce #'matrix-multiplication
          matrixes))


;; Conversions

;; vertex(:=p) conversion
;; f(p) = C p ;; for example, deploy model to the world dimension system.
;; f([p1...pn]) = C [p1...pn]
;; C = conversion matrix = reduced multiplication of multiple conversions
;; 

(defun vec3-horizontal-matrix (vec3)
  (make-array (list 1 3)
              :initial-contents (list vec3)
              ))

(defun vec3-vertical-matrix (vec3)
  (matrix-transpose (vec3-horizontal-matrix vec3)))



(defun deploy-vec3-to-conversion-products (vec3-list)
  ;; deploy vec3 points to C [p1 ... pn]
  ;; pi : (pi.s1, pi.s2, pi.s3, 1)
  (let* ((n-vecs (length vec3-list))
         (current-vec nil)
         (points-array-matrix (make-array (list 4 n-vecs))))
    (loop for i from 0 to (1- n-vecs)
          do (setf current-vec (nth i vec3-list))
             (setf (aref points-array-matrix 0 i) (vec3-s1 current-vec))
             (setf (aref points-array-matrix 1 i) (vec3-s2 current-vec))
             (setf (aref points-array-matrix 2 i) (vec3-s3 current-vec))
             (setf (aref points-array-matrix 3 i) 1))
    points-array-matrix))

;; example
;;>> (deploy-vec3-to-conversion-products 
;;    (list *sample-vec3-a* *sample-vec3-b* (vec3-reverse *sample-vec3-b*)))
;;#2A((3.0 0.5 -0.5)
;;    (4.0 2.0 -2.0)
;;    (-5.0 -1.0 1.0)
;;    (1 1 1))

(defun 4x4-matrix (cm11 cm12 cm13 cm14 cm21 cm22 cm23 cm24 cm31 cm32 cm33 cm34 cm41 cm42 cm43 cm44)
 ;; equal to Affine Conversion
  (make-array 
   (list 4 4)
   :initial-contents
   `((,cm11 ,cm12 ,cm13 ,cm14)
     (,cm21 ,cm22 ,cm23 ,cm24)
     (,cm31 ,cm32 ,cm33 ,cm34)
     (,cm41 ,cm42 ,cm43 ,cm44))))

(defun 4x4-matrix-identity ()
  (4x4-matrix
   1 0 0 0
   0 1 0 0
   0 0 1 0
   0 0 0 1))

(setf (symbol-function 'conversion-matrix) #'4x4-matrix)

(defun conversion-scale (sx sy sz)
  ;; s is scale factor to each dimention
  (conversion-matrix
   sx 0  0  0
   0  sy 0  0
   0  0  sz 0
   0  0  0  1))

(defun conversion-parallel-move (tx ty tz)
  (conversion-matrix
   1 0 0 tx
   0 1 0 ty
   0 0 1 tz
   0 0 0 1))

(defun conversion-rotate-x-axis(theta-x)
  (conversion-matrix
   1 0             0                 0
   0 (cos theta-x) (- (sin theta-x)) 0
   0 (sin theta-x) (cos theta-x)     0
   0 0             0                 1))
  

(defun conversion-rotate-y-axis(theta-y)
  (conversion-matrix
   (cos theta-y)     0 (sin theta-y) 0
   0                 1 0             0
   (- (sin theta-y)) 0 (cos theta-y) 0
   0                 0 0             1))

(defun conversion-rotate-z-axis(theta-z)
  (conversion-matrix
   (cos theta-z) (- (sin theta-z)) 0 0
   (sin theta-z) (cos theta-z)     0 0
   0             0                 1 0
   0             0                 0 1))

(defun conversion-rotate-euler-angle-xyz (theta-x theta-y theta-z)
  ;; rotate euler angle
  ;; Axis Objects X -> Y' -> Z''
  (matrix-multiplications
   (conversion-rotate-x-axis theta-x)
   (conversion-rotate-y-axis theta-y)
   (conversion-rotate-z-axis theta-z)))

(defun conversion-rotate-tait-bryan-angle-xyz (theta-x theta-y theta-z)
  ;; rotate tait brian angle (axis for rotation is common)
  ;; Axis Objecs X -> Y -> Z
  (matrix-multiplications
   (conversion-rotate-z-axis theta-z)
   (conversion-rotate-y-axis theta-y)
   (conversion-rotate-x-axis theta-x)))

(defun conversion-rotate-rodrigues (theta vec-n)
  ;; rodrigues rotation
  ;; vN :: Normalized Vector of Axis(Center for rotation)
  ;; vR :: vector to rotate
  ;; vR' :: vector rotated
  ;; vR' = {cos(theta)*E + (1-cos(theta))*vN*transposed(vN) + sin(theta)*corss(vN, vN)} * vR
  (let* ((c (cos theta))
         (h (- 1 c))
         (s (sin theta))
         (n (vec3-normalize vec-n
                            )))
    (conversion-matrix
     ;; _1i
     (+ (* h (vec3-s1 n) (vec3-s1 n)) c)
     (- (* h (vec3-s2 n) (vec3-s1 n)) (* s (vec3-s3 n)))
     (+ (* h (vec3-s3 n) (vec3-s1 n)) (* s (vec3-s2 n)))
     0
     ;; _2i
     (+ (* h (vec3-s1 n) (vec3-s2 n)) (* s (vec3-s3 n)))
     (+ (* h (vec3-s2 n) (vec3-s2 n)) c)
     (- (* h (vec3-s3 n) (vec3-s2 n)) (* s (vec3-s1 n)))
     0
     ;; _3i
     (- (* h (vec3-s1 n) (vec3-s3 n)) (* s (vec3-s2 n)))
     (+ (* h (vec3-s2 n) (vec3-s3 n)) (* s (vec3-s1 n)))
     (+ (* h (vec3-s3 n) (vec3-s3 n)) c)
     0
     ;; _4i
     0 0 0 1)))


(defun euler-xyz-angle (theta-x theta-y theta-z)
  ;; theta-x :: roll 
  ;; theta-y :: pitch
  ;; theta-z :: yaw
  (vector-3d theta-x theta-y theta-z))

(defun euler-xyz-angle-roll (euler-xyz-angle)
  (vec3-s1 euler-xyz-angle))

(defun euler-xyz-angle-pitch (euler-xyz-angle)
  (vec3-s2 euler-xyz-angle))

(defun euler-xyz-angle-yaw (euler-xyz-angle)
  (vec3-s3 euler-xyz-angle))


;;;; quaternion
;;
;; https://www.mss.co.jp/technology/report/pdf/18-07.pdf
;; https://www.tobynorris.com/work/prog/csharp/quatview/help/orientations_and_quaternions.htm
;; https://github.com/mrdoob/three.js/blob/dev/src/math/Quaternion.js
;;
;; ~q = r, v:{vx, vy, vz} = {r, vx, vy, vz}

(defun quaternion (qr qi qj qk)
  ;; real-part q-r, vector-value q-i q-j q-k
  (make-array (list 4)
              :initial-contents (list (en-scalar-unit qr)
                                      (en-scalar-unit qi) (en-scalar-unit qj) (en-scalar-unit qk))))

(defun quaternion-by-vec3 (real vec3)
  (quaternion real (vec3-s1 vec3) (vec3-s2 vec3) (vec3-s3 vec3)))

(defun quaternionp (q?)
  (and (arrayp q?) 
       (= (array-rank q?) 1)
       (= (array-dimension q? 0) 4)))

(defparameter *sample-quaternion-a* (quaternion 4 0.1 -0.5 0.2))
(defparameter *sample-quaternion-b* (quaternion-by-vec3 4 (vector-3d 2 -6 3)))

(defun quaternion-r (quaternion)
  (aref quaternion 0))

(defun quaternion-i (quaternion)
  (aref quaternion 1))

(defun quaternion-j (quaternion)
  (aref quaternion 2))

(defun quaternion-k (quaternion)
  (aref quaternion 3))

(setf (symbol-function 'q-r) #'quaternion-r)
(setf (symbol-function 'q-i) #'quaternion-i)
(setf (symbol-function 'q-j) #'quaternion-j)
(setf (symbol-function 'q-k) #'quaternion-k)

(defun quaternion-vector-3d (q)
  (vector-3d (q-i q) (q-j q) (q-k q)))

(defun quaternion-scalar (q)
  (q-r q))

(defun quaternion-add (q1 q2)
  (quaternion (+ (q-r q1) (q-r q2))
              (+ (q-i q1) (q-r q2))
              (+ (q-j q1) (q-r q2))
              (+ (q-k q1) (q-r q2))))

(defun quaternion-sub (q1 q2)
  (quaternion (- (q-r q1) (q-r q2))
              (- (q-i q1) (q-r q2))
              (- (q-j q1) (q-r q2))
              (- (q-k q1) (q-r q2))))

(defun quaternion-multiply (quaternion scalar)
  (quaternion (* (q-r quaternion) scalar)
              (* (q-i quaternion) scalar)
              (* (q-j quaternion) scalar)
              (* (q-k quaternion) scalar)))

(defun quaternion-division (quaternion scalar)
  (quaternion (/ (q-r quaternion) scalar)
              (/ (q-i quaternion) scalar)
              (/ (q-j quaternion) scalar)
              (/ (q-k quaternion) scalar)))

(defun quaternion-conjugate (quaternion)
  ;; q*
  (quaternion (q-r quaternion)
              (- (q-i quaternion)) (- (q-j quaternion)) (- (q-k quaternion))))

(defun hamilton-product-matrix (q)
  ;; q is quaternion
  (conversion-matrix
   (+ (q-r q)) (- (q-i q)) (- (q-j q)) (- (q-k q))
   (+ (q-i q)) (+ (q-r q)) (- (q-k q)) (+ (q-j q))
   (+ (q-j q)) (+ (q-k q)) (+ (q-r q)) (- (q-i q))
   (+ (q-k q)) (- (q-j q)) (+ (q-i q)) (+ (q-r q))))

(defun quaternion-product (q p)
  ;; n0 : scaler of n. nv: vector of n.
  ;; q p = (q0 + qv)(p0 + pv)
  (matrix-vector-multiplication
   (hamilton-product-matrix q) p))

(defun quaternion-magnitude^2 (q)
  ;; || ~q || ^2
  (+ (expt (q-r q) 2)
     (expt (q-i q) 2)
     (expt (q-j q) 2)
     (expt (q-k q) 2)))

(defun quaternion-magnitude (q)
  ;; || ~q ||
  (sqrt (quaternion-magnitude^2 q)))

(defun quaternion-inverse (q)
  ;; q^(-1)
  (quaternion-division (quaternion-conjugate q) (quaternion-magnitude^2 q)))

(defun quaternion-normalize (q)
  (quaternion-division q (quaternion-magnitude q)))

(defun quaternion-normalizedp (q)
  ;; 1^2 = 1
  (~= (quaternion-magnitude^2 q) 1))

;; (quaternion-magnitude (quaternion-product *sample-quaternion-a* (quaternion-inverse *sample-quaternion-a*)))
;; ~= 1

;;;; quaternion rotation
;; theta, N. where || N || == 1

(defun rotation-quaternion (quaternion)
  (quaternion-normalize quaternion))

(defun rotation-quaternion-ss (qr qi qj qk)
  (rotation-quaternion (quaternion qr qi qj qk)))

(defun rotation-quaternion-of-rodrigues (theta vec3)
  (quaternion-by-vec3
   (cos (/ theta 2))
   (vec3-multiply (vec3-normalize vec3) (sin (/ theta 2)))))

(defun rotation-quaternion-by-vec3 (theta vec3)
  ;;(rotation-quaternion-of-rodrigues theta vec3))
  (rotation-quaternion 
   (quaternion theta (vec3-s1 vec3) (vec3-s2 vec3) (vec3-s3 vec3))))


(defun rotation-quaternionp (rq?)
  (and (quaternionp rq?)
       (quaternion-normalizedp rq?)))

;;

(defun rotation-quaternion-s-angle-axis (rq)
  ;; on the doOutput "resa" id, axis[0,1,2], angle section of
  ;; https://github.com/gaschler/rotationconverter/blob/master/index.html
  ;; and also
  ;; http://www.euclideanspace.com/maths/geometry/rotations/conversions/quaternionToAngle/
  (let ((theta (* 2 (acos (q-r rq))))
        (axis
          (vec3-division
           (quaternion-vector-3d rq)
           (if (< (- 1 (* (q-r rq) (q-r rq))) +rounding-error-constant+)
               1.00
               (sqrt (- 1 (* (q-r rq) (q-r rq))))))))
    (values theta axis)))


(defun rotation-quaternion-rotate-point (rq)
  ;; r = {x,y,z, (1)}
  ;; r' = q r (conjugate q) = Ar
  ;; ?- A
  (4x4-matrix
   ;; 1i
   (+ (+ (square (q-r rq))) (+ (square (q-i rq))) (- (square (q-j rq))) (- (square (q-k rq))))
   (* 2 (- (* (q-i rq) (q-j rq)) (* (q-r rq) (q-k rq))))
   (* 2 (+ (* (q-i rq) (q-k rq)) (* (q-r rq) (q-j rq))))
   0
   ;; 2i
   (* 2 (+ (* (q-i rq) (q-j rq)) (* (q-r rq) (q-k rq))))
   (+ (+ (square (q-r rq))) (- (square (q-i rq))) (+ (square (q-j rq))) (- (square (q-k rq))))
   (* 2 (- (* (q-j rq) (q-k rq)) (* (q-r rq) (q-i rq))))
   0
   ;; 3i
   (* 2 (- (* (q-i rq) (q-k rq)) (* (q-r rq) (q-j rq))))
   (* 2 (+ (* (q-j rq) (q-k rq)) (* (q-r rq) (q-i rq))))
   (+ (+ (square (q-r rq))) (- (square (q-i rq))) (- (square (q-j rq))) (+ (square (q-k rq))))
   0
   ;; 4i
   0 0 0 1))

(defun rotation-quaternion-rotate-axis-system (rq)
  ;; r' = (conjugate q) r q = Ar
  ;; ?- A
  (matrix-transpose (rotation-quaternion-rotate-point rq)))

(defun diff-time-rotation-quaternion-in-reference-system (q vec3-n dtheta/dt)
  ;; in referenced coordinate system,
  ;; ((d/dt) q) = A q
  ;; ((d/dt) q) = 1/2 omega q
  ;; omega == (dtheta/dt) n
  ;; n is {Rx, Ry, Rz} = vec3 ;; Axis(line of Rolling center, Eigenvector)
  ;; ?- A
  (let ((omega (vec3-multiply vec3-n dtheta/dt)))
    (let ((ox (vec3-s1 omega))
          (oy (vec3-s2 omega))
          (oz (vec3-s3 omega)))
      (matrix-vector-multiplication 
       (4x4-matrix 
        0      (- ox) (- oy) (- oz)
        (+ ox) 0      (- oz) (+ oy)
        (+ oy) (+ oz) 0      (- ox)
        (+ oz) (- oy) (+ ox) 0     )
       q))))

(defun diff-time-rotation-quaternion-in-moving-system (q vec3-n dtheta/dt)
  ;; in moving object coordinate system,
  ;; ((d/dt) q) = A q
  ;; ?- A
  (let ((omega (vec3-multiply vec3-n dtheta/dt)))
    (let ((ox (vec3-s1 omega))
          (oy (vec3-s2 omega))
          (oz (vec3-s3 omega)))
      (matrix-vector-multiplication 
       (4x4-matrix 
        0      (- ox) (- oy) (- oz)
        (+ ox) 0      (+ oz) (- oy)
        (+ oy) (- oz) 0      (+ ox)
        (+ oz) (+ oy) (- ox) 0     )
       q))))

;; ex
;; (rotation-quaternion-rotate-point (rotation-quaternion 1.00 (vec3-normalize *sample-vec3-a*)))

(defun rotation-quaternion-synth-point (rq-list)
  ;; rotate from from nth 1 -> 2 -> ... -> n
  (reduce #'(lambda (mat q) (matrix-multiplication mat (rotation-quaternion-rotate-point q)))
          (reverse rq-list) :initial-value (4x4-matrix-identity)))

(defun rotation-quaternion-synth-axis-system (rq-list)
  ;; rotate from from nth 1 -> 2 -> ... -> n
  (reduce #'(lambda (mat q) (matrix-multiplication mat (rotation-quaternion-rotate-axis-system q)))
          (reverse rq-list) :initial-value (4x4-matrix-identity)))

#|
;; ex
(rotation-quaternion-synth-point
 (list (rotation-quaternion (/ *pi* 2) (vec3-normalize *sample-vec3-a*))
       (rotation-quaternion (/ *pi* 6) (vec3-normalize *sample-vec3-b*))))
|#


;;;; euler-angle and rotation-quaternion


;; check at game section of : https://ken3susume.com/archives/12958

#|
(defun euler-xyz-angle-to-rotation-quaternion (euler-xyz-angle)
  ;; rotation (alpha by X)->(beta by Y')->(gamma in Z'').
  ;; ?- equally quaternion
  (let ((C-a/2 (cos (* 1/2 (euler-xyz-angle-roll euler-xyz-angle))))
        (S-a/2 (sin (* 1/2 (euler-xyz-angle-roll euler-xyz-angle))))
        (C-b/2 (cos (* 1/2 (euler-xyz-angle-pitch euler-xyz-angle))))
        (S-b/2 (sin (* 1/2 (euler-xyz-angle-pitch euler-xyz-angle))))
        (C-g/2 (cos (* 1/2 (euler-xyz-angle-yaw euler-xyz-angle))))
        (S-g/2 (sin (* 1/2 (euler-xyz-angle-yaw euler-xyz-angle)))))
    (quaternion
     (- (* C-a/2 C-b/2 C-g/2) (* S-a/2 S-b/2 S-g/2))
     (+ (* S-a/2 C-b/2 C-g/2) (* C-a/2 S-b/2 S-g/2))
     (- (* C-a/2 S-b/2 C-g/2) (* S-a/2 C-b/2 S-g/2))
     (+ (* C-a/2 C-b/2 S-g/2) (* S-a/2 S-b/2 C-g/2)))))
|#


#|
(euler-xyz-angle
 (atan (* 2.0 (+ (* q2 q3) (* q0 q1)))
       (+ (+ (* q0 q0)) (- (* q1 q1)) (- (* q2 q2)) (+ q3 q3 )))
 theta-y
 (theta-y (asin (* 2.0 (- (* q0 q2) (* q1 q3))))))
(atan (* 2.0 (+ (* q1 q2) (* q0 q3)))
      (+ (+ (* q0 q0)) (+ (* q1 q1)) (- (* q2 q2)) (- q3 q3 ))))
|#


;; this is ZYX
(defun rotation-quaternion-to-euler-zyx-angle (rotation-quaternion)
  (let ((q0 (q-r rotation-quaternion))
        (q1 (q-i rotation-quaternion))
        (q2 (q-j rotation-quaternion))
        (q3 (q-k rotation-quaternion)))
    (euler-xyz-angle
     (atan (* 2 (+ (* q0 q1) (* q2 q3)))
           (- 1 (* 2 (+ (* q1 q1) (* q2 q2)))))
     (asin (* 2 (- (* q0 q2) (* q3 q1))))
     (atan (* 2 (+ (* q0 q3) (* q1 q2)))
           (- 1 (* 2 (+ (* q2 q2) (* q3 q3))))))))

(defun rotation-quaternion-to-euler-xyz-angle (rotation-quaternion)
  (let* ((q0 (q-r rotation-quaternion))
         (q1 (q-i rotation-quaternion))
         (q2 (q-j rotation-quaternion))
         (q3 (q-k rotation-quaternion))
         ;
         (ya (asin (+ (* 2 q2 q0) (* -2 q1 q3)))))
    (if (~= (cos ya) 0)
        ;
        (print "Axis locking.")
        ;
        (euler-xyz-angle
         (atan (+ (* 2 q2 q3) (* 2 q1 q0))
               (+ (* 2 q0 q0) (* 2 q3 q3) -1))
         ;;(asin (+ (* 2 q2 q0) (* -2 q1 q3)))
         ya
         (atan (+ (* 2 q1 q2) (* 2 q0 q3))
               (+ (* 2 q0 q0) (* 2 q1 q1)-1))))))
    

#|
		const cos = Math.cos;
		const sin = Math.sin;

		const c1 = cos( x / 2 );
		const c2 = cos( y / 2 );
		const c3 = cos( z / 2 );

		const s1 = sin( x / 2 );
		const s2 = sin( y / 2 );
		const s3 = sin( z / 2 );

		switch ( order ) {

			case 'XYZ':
				this._x = s1 * c2 * c3 + c1 * s2 * s3;
				this._y = c1 * s2 * c3 - s1 * c2 * s3;
				this._z = c1 * c2 * s3 + s1 * s2 * c3;
				this._w = c1 * c2 * c3 - s1 * s2 * s3;
				break;

|#


#|
    (euler-xyz-angle
     (atan (* 2.0 (+ (* q2 q3) (* q0 q1)))
           (+ (+ (* q0 q0)) (- (* q1 q1)) (- (* q2 q2)) (+ q3 q3 )))
     
     (asin (* 2.0 (- (* q0 q2) (* q1 q3))))
    (atan (* 2.0 (+ (* q1 q2) (* q0 q3)))
          (+ (+ (* q0 q0)) (+ (* q1 q1)) (- (* q2 q2)) (- q3 q3 ))))))
|#

#|
    (let ((theta-y (asin (+ (* 2 q0 q2) (* 2 q1 q3)))))
      (if (not (~= (cos theta-y) 0))
          (euler-xyz-angle
           (atan (- (+ (* 2 q2 q3) (- (* 2 q0 q3))))
                 (+ (* 2 q0 q0) (* 2 q3 q3) -1))
           theta-y
           (atan (- (+ (* 2 q1 q2) (- (* 2 q0 q3))))
                 (+ (* 2 q0 q0) (* 2 q1 q1) -1)))
          ;; otherwise
          (euler-xyz-angle
           (atan (+ (+ (* 2 q2 q3) (* 2 q0 q3)))
                 (+ (* 2 q0 q0) (* 2 q2 q2) -1))
           theta-y
           0)
          ))))

|#









