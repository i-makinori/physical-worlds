;;; movelight.lisp --- Lisp version of movelight.c (Red Book examples)
;;; https://github.com/3b/cl-opengl/blob/master/examples/redbook/movelight.lisp

(in-package :physical-worlds)

;;;;


(defclass test-of-movelight-window (glut:window)
  ((spin :initform 0)
   (euler-camera
    :initform (make-instance 'euler-camera 
                             :focussing (vector-3d 0 0 0)
                             :distance 8
                             :euler-xyz-angle (euler-xyz-angle 0 (* -1/2 *pi*) (* 1/4 *pi*))
                             ))
   (obj-sphere :initform (make-instance 'sphere :radius 0.2)))
  (:default-initargs :width 500 :height 500 :pos-x 100 :pos-y 100
                     :mode '(:single :rgb :depth) :title "movelight.lisp"))


(defmethod glut:display-window :before ((w test-of-movelight-window))
  ;;
  (gl:clear-color 0 0 0 0)
  (gl:shade-model :smooth)
  ;; for text

  ;;
  (gl:enable :lighting)
  (gl:enable :light0)
  (gl:enable :depth-test))

;;; Here is where the light position is reset after the modeling
;;; transformation (GL:ROTATE) is called.  This places the
;;; light at a new position in world coordinates.  The cube
;;; represents the position of the light.
(defmethod display-lights ((w test-of-movelight-window) spin)
  ;; glClear( GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
  (gl:clear :color-buffer-bit :depth-buffer-bit)
  (gl:with-pushed-matrix
    (gl:rotate spin 1 0 0)
    (progn
      (gl:light :light0 :position #(0 0 1.5 1)))
    (progn
      (gl:translate 0 0 1.5)
      (gl:disable :lighting)
      (gl:color 0 1 1)
      (glut:wire-cube 0.1)
      (gl:enable :lighting))))

(defmethod glut:display ((w test-of-movelight-window))
  (gl:clear :color-buffer :depth-buffer)
  ;; camera 
  (look (slot-value w 'euler-camera))
  ;; world
  (gl:with-pushed-matrix
    ;;
    (display-lights w (slot-value w 'spin))
    ;; (gl:light :light1 :position #(1 1 1 1))
    (gl:material :front-and-back :ambient-and-diffuse *color-white*)
    (glut:wire-torus 0.275 0.85 20 20)
    ;;
    (projection (slot-value w 'obj-sphere))
    ;;
    (project-xyz-axis)
    (gl:with-pushed-matrix
      (gl:rotate 20 1 1 1)
      (gl:translate 0.5 -0.5 0.2)
      (project-xyz-axis :scale 2.00))
    (gl:with-pushed-matrix
      (gl:rotate 40 -1.5 -2 4)
      (gl:translate -0.15 -0.15 0.2)
      (project-xyz-axis :scale 0.200))
    ;;
    (project-vector (vector-3d 0 -0.5 -0.5) (vector-3d 1 1 1))
    (project-line (vector-3d 1 1 2) (vector-3d 0.5 1.2 -4))
    
    ;;
    )

  (draw-string-2d "0.2, 0.2" 0.2 0.2)
  (draw-string "foobar" 1.0 0.1 0.1)
  ;; perspect and project
  (reperspect! (slot-value w 'euler-camera) (glut:get :window-width) (glut:get :window-height))
  (gl:flush))

(defmethod reshape ((w test-of-movelight-window))
  (let ((camera (slot-value w 'euler-camera))
        (width (glut:get :window-width))
        (height (glut:get :window-height))
        (pad 5))
    (if (and (> width (* pad 2)) (> height (* pad 2)))
        (gl:viewport pad pad (- width pad pad) (- height pad pad))
        (gl:viewport 0 0 0 0))
    (reperspect! camera (glut:get :window-width) (glut:get :window-height))
    ))

(defmethod glut:reshape ((w test-of-movelight-window) width height)
  (reshape w))

(defmethod glut:mouse ((w test-of-movelight-window) button state x y)
  (declare (ignore x y))
  (when (and (eq button :left-button) (eq state :down))
    (with-slots (spin) w
      (setf spin (mod (+ spin 30) 360)))
    (glut:post-redisplay)))

(defmethod glut:mouse-wheel ((w test-of-movelight-window) button dir x y)
  button x y
  (cond ((eq dir :up)         (fovy-zoom (slot-value w 'euler-camera) +1.00))
        ((eq dir :wheel-down) (fovy-zoom (slot-value w 'euler-camera) -1.00))
        (t nil))
  (glut:post-redisplay)
  )

(defparameter *delta-theta* (/ *pi* 32))

(defmethod glut:keyboard ((w test-of-movelight-window) key x y)
  (declare (ignore x y))
  (if (eql key #\Esc) (glut:destroy-current-window)
      (progn
        (if (eql key #\m)
            (with-slots (spin) w
              (setf spin (mod (+ spin 30) 360))))
        (let ((camera (slot-value w 'euler-camera)))
          (case key
            (#\a (yaw-camera camera (- *delta-theta*))) ;; yaw
            (#\d (yaw-camera camera (+ *delta-theta*)))
            (#\w (pitch-camera camera (- *delta-theta*))) ;; pitch
            (#\s (pitch-camera camera (+ *delta-theta*)))
            (#\e (roll-camera camera (- *delta-theta*))) ;; roll
            (#\z (roll-camera camera (+ *delta-theta*)))))
  (glut:post-redisplay))))

(defun test-of-movelight ()
  (glut:display-window (make-instance 'test-of-movelight-window)))


