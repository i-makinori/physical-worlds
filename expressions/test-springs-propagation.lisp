(in-package :physical-worlds)



;;;; rigid




;;;; dynamic object

(defparameter *d/dt-equal-zero-vec3* 
  #'(lambda (obj time) obj time *zero-vec3*))

(defparameter *d/dt-position-equal-position*
  #'(lambda (obj time) obj time (obj-position obj)))

(defclass dynamic-object (sphere) ;; also domain-object
  ((forced-position
    :initarg :forced-position
    :initform *d/dt-position-equal-position*)
   (forced-velocity
    :initarg :forced-velocity
    :initform *d/dt-equal-zero-vec3*)
   (forced-acceleration
    :initarg :forced-acceleration
    :initform *d/dt-equal-zero-vec3*)))


;; init

(defun init-dynamic-object (mass position0 velocity0 acceleration0
                            &key
                            ;; forced vibrations
                            (forced-position *d/dt-position-equal-position*)
                            (forced-velocity *d/dt-equal-zero-vec3*) 
                            (forced-acceleration *d/dt-equal-zero-vec3*))
  ;; testcase : (init-dynamic-object #1A(1 2 3) #1A(0 -1 -3) #1A(2 -2 3) 1)
  (make-instance 
   'dynamic-object 
   :mass mass
   :radius 0.1
   :position position0 :velocity velocity0 :acceleration acceleration0
   :forced-position forced-position
   :forced-velocity forced-velocity
   :forced-acceleration forced-acceleration))

;; update

(defmethod forced-vibration-position ((obj dynamic-object) time)
  (with-slots (position mass forced-position) obj
    ; forced- position
    (setf position
          (vec3-add *zero-vec3* (funcall forced-position obj time)))
    obj))

(defmethod forced-vibration-velocity ((obj dynamic-object) time)
  (with-slots (velocity mass forced-velocity) obj
    ; forced velocity input. for only this timestep. R = R + dt*v
    (setf velocity
          (vec3-add velocity 
                    (vec3-multiply (funcall forced-velocity obj time) *time-step*)))
    obj))

(defmethod forced-vibration-acceleration ((obj dynamic-object) time)
  (with-slots (acceleration mass forced-acceleration) obj
    ; forced acceleration input. for only this timestep.
    (add-force-for-time-step! obj
                              (vec3-multiply (funcall forced-acceleration obj time)
                                             (* mass (/ 1 *time-step*))))
    obj))

(defparameter *viscosity-coefficient* 0.08)
(defparameter *gravity* #1A(0 0 0)) ; init as zero gravity

(defmethod update-forces ((obj dynamic-object) time)
  ;; add forces

  ;; viscous resistance (resistance by velocity)
  (with-slots (velocity mass) obj
    (add-force-for-time-step! obj
                              (vec3-multiply velocity (- *viscosity-coefficient*))))

  ;; gravity force
  (add-force-for-time-step! obj *gravity*)

  ;; force by spring 
  ; it is defined by update-forces of spring.
  *zero-vec3*

  ;; forced vibration
  ; it is inputed by whole-objects loop
  *zero-vec3*

  ;; return
  obj)



(defmethod update-object ((obj dynamic-object))
  ;; and update
  (update-object-physically obj))

;; (defun project-dynacmic-object (object)
;; )

;;;; spring object

(defclass spring-object (lay-vector) ;; also domain-object. tube or lay vector is better than OBB
  ((natural-length
    :initarg :natural-length
    :initform 1.00)
   (spring-constant
    :initarg :spring-constant
    :initform 0.01)
   (connects-a
    :initarg :connects-a
    :initform *zero-domain*)
   (connects-b
    :initarg :connects-b
    :initform *zero-domain*)))

;; init

(defun init-spring-object (natural-length spring-constant
                           object-connects-a object-connects-b
                           &key (mass 1.00))
  ;;
  (make-instance
   'spring-object
   :mass mass
   :natural-length natural-length :spring-constant spring-constant
   :connects-a object-connects-a :connects-b object-connects-b))

;; update

(defmethod update-forces ((spring spring-object) time)
  ;; hooke's law
  ;; | F | = k * delta_length
  ;; F(a) = | F | * normal_vec(R(a) \{plus_or_minus} R(b)) ;; (+) ? or (-) ?
  (with-slots (natural-length spring-constant connects-a connects-b) spring
    (with-slots ((position-a position)) connects-a
      (with-slots ((position-b position)) connects-b
        (let* ((r_a-b (vec3-sub position-a position-b)) ;; relative position
               (direct (vec3-normalize r_a-b))
               (dist (vec3-magnitude r_a-b))
               ;;
               (delta_length (- dist natural-length))
               (force (* spring-constant delta_length)))
          (add-force-for-time-step! connects-a 
                                    (vec3-multiply (vec3-reverse direct) force))
          (add-force-for-time-step! connects-b
                                    (vec3-multiply (identity direct) force))
          ;;(format t  "force is ~A ~%" force)
  )))))


;; project

(defmethod projection ((spring spring-object))
  (with-slots (connects-a connects-b) spring
    (with-slots ((r-a position)) connects-a
      (with-slots ((r-b position)) connects-b
        (project-vector r-a (vec3-sub r-b r-a))))))



;;;; static object 

(defclass static-object (obb) ;; also domain-object.
  ())

;; init

(defun init-static-object (mass position)
  (make-instance 'static-object
                 :mass mass
                 :width-height-depth #1A(0.2 0.2 0.2)
                 :position position))

;; update

(defmethod update-forces ((obj static-object) time)
  )

(defmethod update-object ((obj static-object))
  ;; it is static
  (with-slots (position velocity acceleration) obj
    (setf position position)
    (setf velocity *zero-vec3*)
    (setf acceleration *zero-vec3*))
  obj)

;; project 

nil


;;;; spring, static, dynamic, objects

(defparameter *whole-objects* nil)

(defun init-springses (init-objects-function)
  (setf *whole-objects*
        ;(coerce (funcall #'_whole-objects-initial-nawatobi_) 'vector)))
        (coerce (funcall init-objects-function) 'vector)
        ))


(defun update-springses (objects-array time)
  (progn
    ;; forced vibration velocity and acceleration
    (loop for obj being the elements of objects-array
          do (when (typep obj 'dynamic-object)
               (forced-vibration-acceleration obj time)
               (forced-vibration-velocity obj time)))
    ;; loop for point masses
    (loop for obj being the elements of objects-array
          do (when (or (typep obj 'dynamic-object)
                       (typep obj 'static-object))
               (update-forces obj time)))

    ;; loop for springs
    (loop for obj being the elements of objects-array
          do (when (typep obj 'spring-object)
               (update-forces obj time)))
    ;; update-all
    (loop for obj being the elements of objects-array
          do (update-object obj))
    ;; forced vibration posision
    (loop for obj being the elements of objects-array
          do (when (typep obj 'dynamic-object)
               (forced-vibration-position obj time)))
    ;; return
    objects-array))

(defun project-springses (objects-array)
  (progn
    (loop for obj being the elements of objects-array
          do (projection obj))  
  ))

;;;;



(defclass springs-model (model)
  ((object-series
    :accessor spring-object-series
    :initform (init-springses #'_whole-objects-initial-1_)
    ;;'() ;; springs, rigits, stationary-objects Array here
    )
   (focussing-sphere
    :accessor spring-focussing-sphere
    :initform (make-instance 'sphere :radius 0.05 :position (vector-3d 1.2 0.8 0.5)))))



;;;;


(defclass test-springs-window (simulator-window)
  ((model :initform (make-instance 'springs-model))
   )
  (:default-initargs :title "handle-event window"))


(defmethod glut:display-window :before ((w test-springs-window))
  (setf (slot-value (displays-camera w) 'focussing)
        (spring-focussing-sphere (displays-model w))
        )
  (with-slots ((disps-time displays-timer)) w
    (setf (model-timer (displays-model w))
          disps-time)
    (setf (slot-value disps-time 'counter-stoppingp)
          t)
    (setf (slot-value disps-time 'time-ago)
          0))
  (init-simulator-window w)
  )

;;;;

(defparameter *delta-theta* (/ *pi* 96))

 
(defmethod update-displaying-by-input ((w test-springs-window))
  (let ((wrapper (input-wrapper w))
        (width (glut:get :window-width))
        (height (glut:get :window-height)))
    ;; camera
    (let* ((camera (displays-camera w)))
      (with-slots (mouse-left mouse-middle mouse-xy-delta mouse-wheel) wrapper
        (if (eql mouse-wheel :up)         (fovy-zoom camera +1.00))
        (if (eql mouse-wheel :wheel-down) (fovy-zoom camera -1.00)))
      (manipulate-camera-wraparound-by-mouse-move camera wrapper width height 32 
                                                  'mouse-right 'mouse-middle)
      (manipulate-camera-wraparound-by-key camera wrapper 0.01 :roll
                                           #\j #\k ))
    ;; object sphere
    (let ((sph (spring-focussing-sphere (displays-model w))))
      (manipulate-obj-rotation-by-key sph wrapper *delta-theta*
                                      #\e #\z #\a #\d #\w #\s)
      (manipulate-obj-move-xyz-axis-by-key sph wrapper 0.02
                                           :key-left :key-right :key-down :key-up #\m #\n))
    ;; and more handling
    (with-slots (keys) wrapper
      ;; time step
      (with-slots (configured-time-step) (displays-timer w)
        (if (key-just-pressed? #\p keys)
            (switch-call-or-stop-timer (displays-timer w))) ;; pause and restart
        (if (key-just-pressed? #\o keys)
            (setf configured-time-step (* configured-time-step (/ 1 1.05))))
        (if (key-just-pressed? #\i keys)
            (setf configured-time-step (* configured-time-step (* 1 1.05))))
        (if (key-pressing? #\u keys) (incf (slot-value (displays-timer w) 'frame-counter)))
      ))))


(defmethod en-projector-from-model ((w test-springs-window))
  (let ((obj-sph (spring-focussing-sphere (displays-model w))))
    (with-slots (displays-timer) w
      (with-slots (frame-counter) displays-timer
        (gl:with-pushed-matrix
          ;;
          (projection obj-sph)
          ;(projection-wire obj-sph)
          (gl:material :front-and-back :ambient-and-diffuse *color-mediumpurple*)
          (projection-slots-params obj-sph
                                   :xyz-axis t :rotation-axis t)

          (with-slots (position) obj-sph
            (project-vector position (object-forward obj-sph) :color *color-red*)
            (project-vector position (object-up obj-sph) :color *color-green*)
            (project-vector position (object-right obj-sph) :color *color-blue*))

          (project-xyz-axis)
          ;;
          (with-slots (object-series) (displays-model w)
            (project-springses object-series))
          ;;
          ;(gl:with-pushed-matrix
          ;(gl:translate (* (cos (* 0.02 frame-counter)) 1) (* (sin (* 0.02 frame-counter)) 1) 0)
          ;(glut:wire-cube 0.5))
          )))))


(defmethod draw-status-texts ((w test-springs-window))
  w
  (gl-color *color-white*)
  (draw-string-2d (format-timer-template-status (displays-timer w)) 0.7 0.9)
  (draw-string-2d (format-input-status (input-wrapper w)) 0.7 0.5)
  ;;(draw-string-2d (format-camera-status (displays-camera w)) 0.02 0.9)
  (draw-string-2d (describe-object-to-string (spring-focussing-sphere (displays-model w))) 0.02 0.5)
  ;;
  #|
  (with-slots ((objs object-series)) (displays-model w)
    (draw-string-2d
     (apply #'concatenate 'string
            (loop for obj being the elements of objs  
                  collect (format nil "R:~,5F, V:~,5F, A:~,5F~%"
                                  (obj-position obj)
                                  (obj-velocity obj)
                                  (obj-acceleration obj))))
     0.02 0.95))
     |#
     )


;;;;


(defun update-world-by-input (objects input)
  objects input
  )


;;(defmethod update-world ()
(let ((frame-ago 0))
  (defmethod update-model ((m springs-model))
    ;; update worlds
    #|
    (with-slots (model input-wrapper) w
      (update-world-by-input model input-wrapper))
    |#
    ;;

    (with-slots (object-series timer) m
      (with-slots (frame-counter counter-stoppingp time-ago) timer
        (unless (and counter-stoppingp
                     (= frame-ago frame-counter))
          (incf time-ago *time-step*)
          (setf *time-step* (slot-value timer 'configured-time-step))
          (update-springses object-series time-ago))
        (setf frame-ago frame-counter)
))))





;;;; timer manager

(defparameter *gravity-zero* #1A(0 0 0))
(defparameter *gravity-earth* #1A(0 -9.8 0))


(defun test-of-springs-model (&key 
                              (viscosity-coefficient 0.20)
                              (gravity *gravity-zero*)
                                   
                                   (init-objects-function #'_whole-objects-initial-1_))
  (setf *viscosity-coefficient* viscosity-coefficient)
  (setf *gravity* gravity)
  (let ((window (make-instance 'test-springs-window)))
    ;; model's initial-objecs-array
    (setf (spring-object-series
           (slot-value window 'model))
          (init-springses init-objects-function))
    ;; timer
    (timer-s-params-set window 0)
    (call-timer window 0)
    ;; key
    (glut:set-key-repeat :key-repeat-off)
    ;;
    (glut:display-window window)
    ))

;; example useages
;; another examples are refered at test-springs-propagation-example-waves.lisp
#|
;; none Attenuation model
(test-of-springs-model :viscosity-coefficient 0.0
                       :gravity *gravity-earth*
                       :init-objects-function #'_whole-objects-initial-1_)
;; nawatobi sample
(test-of-springs-model :gravity *gravity-earth* 
                       :init-objects-function #'_whole-objects-initial-nawatobi_)

;; circled sample
(test-of-springs-model :viscosity-coefficient 0.2
                       :gravity *gravity-earth*
                       :init-objects-function #'_whole-objects-initial-delta-star-0_)

;; matrixd wave
(test-of-springs-model :gravity *gravity-zero* 
                       :init-objects-function #'_whole-objects-initial-matrix-0_)

;; Oosakajime matrix
(test-of-springs-model :viscosity-coefficient 0.4
                                        :gravity *gravity-zero*
                                        :init-objects-function #'_whole-objects-initial-matrix-oosakajime_)

|#

