;; test for event handling

(in-package :physical-worlds)


;;;; test handler window

(defclass handler-model (model)
  ((objects-series
    :initform `#(,(make-instance 'sphere :radius 0.2 :position (vector-3d 1.2 0.8 0.5))))))

(defclass test-handler-window (simulator-window)
  ((model :initform (make-instance 'handler-model))
   )
  (:default-initargs :title "handle-event window"))


(defmethod access-sphere ((w test-handler-window))
  ;(defmacro access-sphere (w)
  ;  `(aref (model-objects (displays-model ,w)) 0))
  (aref (model-objects (displays-model w)) 0))

(defmethod glut:display-window :before ((w test-handler-window))
  ; 6/21 ヌーソロジーでは
  ; 観測者が居なくては、世界はなりたたない
  ; 観測者が居なくなったら、世界も終わる。
  ; 観測者が動いているのではなく、世界が動いている。
  ;
  ;(setf (slot-value (displays-camera w) 'position)
  ;(slot-value (access-sphere w) 'position))

  ; 天動説と地動説の変換。
  ; 2つの物体からみて、どちらが回っているのか、
  (setf (slot-value (displays-camera w) 'focussing)
        (access-sphere w))

  (init-simulator-window w))

;;;;

(defparameter *delta-theta* (/ *pi* 96))

 
(defmethod update-displaying-by-input ((w test-handler-window))
  (let ((wrapper (input-wrapper w))
        (width (glut:get :window-width))
        (height (glut:get :window-height)))

    ;; camera
    (let ((camera (displays-camera w)))
      (with-slots (mouse-left mouse-middle mouse-xy-delta mouse-wheel) wrapper
        (if (eql mouse-wheel :up)         (fovy-zoom camera +1.00))
        (if (eql mouse-wheel :wheel-down) (fovy-zoom camera -1.00)))
      (manipulate-camera-wraparound-by-mouse-move camera wrapper width height 32 
                                                  'mouse-right 'mouse-middle)
      (manipulate-camera-wraparound-by-key camera wrapper 0.01 :roll
                                           #\j #\k ))
    ;; object sphere
    (let ((sph (access-sphere w)))
      (manipulate-obj-rotation-by-key sph wrapper *delta-theta*
                                      #\e #\z #\a #\d #\w #\s)
      (manipulate-obj-move-xyz-axis-by-key sph wrapper 0.02
                                           :key-left :key-right :key-down :key-up #\m #\n))
    ;; and more handling
    (with-slots (keys) wrapper
      ;; scale
      (let* ((timer (slot-value (displays-timer w) 'frame-counter)))
        (with-slots (scale) (access-sphere w)
          (setf scale 
                (cond ((key-pressing? #\u keys)
                       (vector-3d (+ 1 (* 0.8 (sin (* timer 0.015))))
                                  (+ 1 (* 0.3 (cos (* timer 0.020))))
                                  (+ 1 (* 0.5 (sin (* timer 0.010))))))
                      (t (vector-3d 1 1 1))))))
      ;; time step
      (with-slots (configured-time-step) (displays-timer w)
        (if (key-just-pressed? #\p keys)
            (switch-call-or-stop-timer (displays-timer w))) ;; pause and restart
        (if (key-pressing? #\o keys) (setf configured-time-step (* configured-time-step (/ 1 1.05))))
        (if (key-pressing? #\i keys) (setf configured-time-step (* configured-time-step (* 1 1.05))))
      ))))


(defmethod en-projector-from-model ((w test-handler-window))
  (let ((obj-sph (access-sphere w)))
    (with-slots (displays-timer) w
      (with-slots (frame-counter) displays-timer
        (gl:with-pushed-matrix
          ;;
          (projection obj-sph)
          ;(projection-wire obj-sph)
          (gl:material :front-and-back :ambient-and-diffuse *color-mediumpurple*)
          (projection-slots-params obj-sph
                                   :xyz-axis t :rotation-axis t)

          (with-slots (position) obj-sph
            (project-vector position (object-forward obj-sph) :color *color-red*)
            (project-vector position (object-up obj-sph) :color *color-green*)
            (project-vector position (object-right obj-sph) :color *color-blue*))

          (project-xyz-axis)
          ;;
          (gl:with-pushed-matrix
            (gl:translate (* (cos (* 0.02 frame-counter)) 1) (* (sin (* 0.02 frame-counter)) 1) 0)
            (glut:wire-cube 0.5)
            ))))))




(defmethod draw-status-texts ((w test-handler-window))
  w
  (gl-color *color-white*)
  #|
  ;; check screen (2D) position
  (loop for i from 0 to 10
        do (loop for j from 0 to 10
                 do (draw-string-2d (format nil "(~A,~A)" i j)
                                    i j 20 20)))
  |#
  (draw-string-2d (format-timer-template-status (displays-timer w)) 0.7 0.9)
  (draw-string-2d (format-input-status (input-wrapper w)) 0.7 0.5)
  (draw-string-2d (format-camera-status (displays-camera w)) 0.02 0.9)
  (draw-string-2d (describe-object-to-string (access-sphere w)) 0.02 0.5)
  ;;
  (draw-string "foobar" 0.8 0.1 0.2))


;;;;


(defun update-world-by-input (objects input)
  objects input
  )


(defmethod update-model ((m handler-model))
  ;; update worlds
  #|
  (with-slots (model input-wrapper) w
    (update-world-by-input model input-wrapper))
  |#
  ;;
  ;; new = lim (d_t->0) (objects(t+d_t) - objects(t) / dt
)





;;;; timer manager


(let* ((window nil)
       (time-ago 0))
  (defun timer-s-params-set (current-window time)
    (setf window current-window)
    (setf time-ago time))
  (cffi:defcallback timer-manager :void ((counter :int))
    (declare (ignore counter))
    (when window
      (with-slots (counter-stoppingp frame-counter) (displays-timer window)
        (when (not counter-stoppingp)
          (setf frame-counter
                (+ frame-counter 1))
          (redisplay window))
        (setf time-ago (/ (what-time-of (displays-timer window))
                          1000))
        (call-timer window time-ago)))))


(defun call-timer (w time-ago)
  "worlds timer"
  ;; 
  (let* ((configured-time-step (slot-value (displays-timer w) 'configured-time-step))
         (ago time-ago)
         (now (/ (what-time-of (displays-timer w))
                 1000))
         (dt (- now ago))
         (wait (max 1
                    (floor (* 1000 (- configured-time-step dt))))))
    ;;
    (update-model (displays-model w))
    ;;
    (glut:timer-func wait (cffi:callback timer-manager) 
                     0)
    ;;
    ))

;;

(defun test-of-handler ()
  (let ((window (make-instance 'test-handler-window)))
    ;; timer
    (timer-s-params-set window 0)
    (call-timer window 0)
    ;; key
    (glut:set-key-repeat :key-repeat-off)
    ;;
    (glut:display-window window)
    ))

;; profile


(defun profile-test-of-hander ()
  (sb-profile:unprofile)
  (sb-profile:profile "PHYSICAL-WORLDS"
                      "CFFI"
                      "CL-GLU"
                      "CL-GLUT"
                      )
  (time
   (test-of-handler))
  (sb-profile:report :print-no-call-list nil :limit 40)
  (sb-profile:unprofile))

;;(ql:quickload :sb-sprofile)
#|
(defun profile-test-of-hander ()
  (sb-sprof:start-profiling)
  (progn (test-of-handler))
  (sb-sprof:stop-profiling)
  (sb-sprof:report))
|#