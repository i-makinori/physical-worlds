(in-package :physical-worlds)

;;;; collision detection


(defmacro flip-values (&body proc)
  (let ((v1 (gensym))
        (v2 (gensym)))
    `(multiple-value-bind (,v1 ,v2)
         (progn ,@proc)
       (values ,v2 ,v1))))


#|

I I I I I I 
F I I I I I 
F F I I I I 
F F F I I I 
F F F F I I 
F F F F F I 

|#

(defmethod collision-detection (obj1-t obj2-t)
  ;(warn (format nil "collision-detection of ~A and ~A is not defined." obj1-t obj2-t))
  (values nil nil))

(defmethod collision-detection ((obj1 lay-vector) (obj2 plane))
  (collision-detection-lay-vector-plane obj1 obj2))

(defmethod collision-detection ((obj1 plane) (obj2 lay-vector))
  (flip-values (collision-detection-lay-vector-plane obj2 obj1)))

(defmethod collision-detection ((obj1 lay-vector) (obj2 sphere))
  (collision-detection-lay-vector-sphere obj1 obj2))

(defmethod collision-detection ((obj1 sphere) (obj2 lay-vector))
  (flip-values (collision-detection-lay-vector-sphere obj2 obj1)))

(defmethod collision-detection ((obj1 lay-vector) (obj2 triangle))
  (collision-detection-lay-vector-triangle obj1 obj2))

(defmethod collision-detection ((obj1 triangle) (obj2 lay-vector))
  (flip-values (collision-detection-lay-vector-triangle obj2 obj1)))

(defmethod collision-detection ((obj1 sphere) (obj2 triangle))
  ; for moving sphere
  (collision-detection-sphere-triangle obj1 obj2))

(defmethod collision-detection ((obj1 triangle) (obj2 sphere))
  ; for moving sphere
  (flip-values (collision-detection-sphere-triangle obj2 obj1)))

(defmethod collision-detection ((obj1 triangle) (obj2 triangle))
  (collision-detection-triangle-triangle obj1 obj2))

(defmethod collision-detection ((obj1 sphere) (obj2 sphere))
  (collision-detection-sphere-sphere obj1 obj2))

(defmethod collision-detection ((obj1 plane) (obj2 sphere))
  (collision-detection-plane-sphere obj1 obj2))

(defmethod collision-detection ((obj1 sphere) (obj2 plane))
  (flip-values (collision-detection-plane-sphere obj2 obj1)))

(defmethod collision-detection ((obj1 obb) (obj2 obb))
  (collision-detection-obb-obb obj1 obj2))




;(defmethod collision-detection ((obj1 lay) (obj2 triangle))
;)




#|

I 0 0 ... 0 0 0
C I 0 ... 0 0 0
C C I ... 0 0 0
 ...  ...  ...
C C C ... I 0 0
C C C ... C I 0
C C C ... C C I

;; all of 0 and C is maybely collisioning.
;; only C or 0 is needed to detect.
|#


(defun collision-detection-whole-each! (objects collisioning-array)
  (let* ((len (length objects))
         ;(collisioning-array (make-array (list len) :initial-element '())))
         )
    collisioning-array
    (loop 
      for i from 0 to (1- len)
      do (loop 
           for j from 0 to (- i 1)
           do (multiple-value-bind (col1 col2)
                  (collision-detection (aref objects i) (aref objects j))
                (if col1
                    (setf (aref collisioning-array i) (cons col1 (aref collisioning-array i))))
                (if col2
                    (setf (aref collisioning-array j) (cons col2 (aref collisioning-array j)))))))
    collisioning-array))

;;; 

;; AABBCC
;; vector, sphere, plane -> polygon -> {OOB, poly-polygon-model} -> multi-joint-model
;; OOB: Object Oriented Box

;; Vector, Sphere, Plane, Tube, Polygon. (10 patterns of) collision detection is needed for premitive.


(defun collision-detection-manager (collision-able-lists)
  collision-able-lists
  )

(defun collision-for-all (object multi-polygon)
  object multi-polygon nil
)

(defun collision-detection-lay-vector-plane (lay-vector plane)
  ;; https://knzw.tech/raytracing/?page_id=78
  ;; (p-c) dot n = 0 ; plane
  ;; p = s + t*d ; lay vector
  ;; p is crossing point
  (with-slots ((s position) (d lay-to)) lay-vector
    (with-slots ((c position) (n normal)) plane
      (let ((d_dot_n (vec3-dot d n))
            (c-s_dot_n (vec3-dot (vec3-sub c s) n)))
        (cond ((~= 0 d_dot_n) ; parallel
               (values nil nil))
              (t
               (let ((time (/ c-s_dot_n d_dot_n)))
                 (values (cons plane time)
                         (cons lay-vector time)))
               ; (0<t<=1) => plane and lay is crossed between time t to t+d_t.
              ))))))


(defun collision-detection-lay-vector-sphere (lay-vector sphere)
  ;; https://knzw.tech/raytracing/?page_id=78
  ;; |p-c|^2 = r^2 ;; sphere
  ;; p = s+t*d ;; lay-vector
  ;; a*t^2+b*t+c=0
  ;; ?:x
  (with-slots ((s position) (d lay-to)) lay-vector
    (with-slots ((c position) (r radius)) sphere
      (let* ((a (vec3-magnitude^2 d))
             (b (* 2 (vec3-dot d (vec3-sub s c))))
             (c (- (vec3-magnitude^2 (vec3-sub s c)) (* r r)))
             (xs (solve-quadratic-coefficient a b c)))
        (cond ((null xs) ;; vec-to is 0 vector
               (values nil nil)) 
              ((complexp (car xs)) ;; not corssed
               (values nil nil))
              (t
               (values (cons sphere xs)
                       (cons lay-vector xs))))))))

(defun collision-detection-lay-vector-triangle (lay-vector triangle)
  ;; http://www.sousakuba.com/Programming/gs_hittest_point_triangle.html
  (with-slots (lay-to (lay-from position)) lay-vector
    (with-slots (>normal (v-a position) (v-b position2) (v-c position3)) triangle
      (let* ((inner-plane (triangle-inner-plane triangle))
             (div-val ; division value
               (cdr (collision-detection-lay-vector-plane lay-vector inner-plane))))
        (if (and (numberp div-val) (< 0 div-val) (<= div-val 1)) ; if vector acrosses triangle's plane
            (let* ((div-p ; division point
                     (vec3-add
                               (vec3-multiply lay-from 1)
                               (vec3-multiply lay-to div-val)))
                   (v-ab (vec3-sub v-b v-a))
                   (v-bc (vec3-sub v-c v-b))
                   (v-ca (vec3-sub v-a v-c))
                   ; cross from point a,b,c
                   (c-a (vec3-cross v-ca (vec3-sub div-p v-a)))
                   (c-b (vec3-cross v-ab (vec3-sub div-p v-b))) 
                   (c-c (vec3-cross v-bc (vec3-sub div-p v-c)))
                   ; if all of angle (orthant) of cross same, the point is inner triangle
                   (dot-ab (vec3-dot c-a c-b))
                   (dot-ac (vec3-dot c-a c-c)))
              #| ; exam: check for vector position
              (project-vector *zero-vec3* div-p)
              (project-vector v-a v-ab)
              (project-vector v-b v-bc)
              (project-vector v-c v-ca)
              (project-vector v-a >normal)
              |#
              (if (and (>= dot-ab 0) (>= dot-ac 0)) ; boundary is inside of triangle
                  (values (cons triangle div-val) ; in point
                          (cons lay-vector div-p))
                  (values nil nil) ; not  in triangle
                          ))
            (values nil nil) ; not in plane
            )))))


(defun collision-detection-sphere-triangle (sphere triangle)
  ;; collision detection of static sphere and static triangle
  ;; if collisioning,
  ;; returns triangle, push back vector and amount of digging for sphere
  ;; returns sphere and collisioning point for triangle
  (with-slots ((sp position) (r radius)) sphere
    (with-slots ((tp position) >normal) triangle
      (let* ((n (vec3-normalize >normal))
             (triangle-plane (triangle-inner-plane triangle))
             (sphere-lay (make-instance 'lay-vector 
                                        :position (vec3-add sp (vec3-reverse (vec3-multiply n r)))
                                        :lay-to (vec3-multiply n (* 2 r)))))
        (multiple-value-bind (cons-triangle-point cons-lay-time)
            (collision-detection-lay-vector-plane sphere-lay triangle-plane)
          (if (cdr cons-triangle-point) ; if parallel
              (let* ((plane-point (cdr cons-triangle-point))
                     (push-back (vec3-multiply n (* 2 r (cdr cons-lay-time))))
                     (dist-to-wire (triangle-distance-to-wire triangle sp)))
                (cond 
                  ((collision-detection-lay-vector-triangle sphere-lay triangle)
                   (values ;(cons triangle push-back)
                    (list triangle push-back (cdr cons-lay-time))
                    (cons sphere plane-point)))
                  ((> r dist-to-wire) 
                   ;; pushing vector for boundary is common to inner triangle's pushing vector now.     
                   ;; todo is push from triangle's edge to sphere's center.
                   (values
                    (list triangle push-back (cdr cons-lay-time))
                    (cons sphere plane-point)))
                  (t
                   (values nil nil))))))))))

(defun collision-detection-triangle-triangle (triangle1 triangle2)
  ; ref: http://www.cg.ces.kyutech.ac.jp/lecture/cg2/cg2-09_collision.pdf
  ; collision detection is ignored if the only-one's plane contains both of triangles.
  (with-slots ((t1_p1 position) (t1_p2 position2) (t1_p3 position3)) triangle1
    (with-slots ((t2_p1 position) (t2_p2 position2) (t2_p3 position3)) triangle2
      (let (;(t1_plane (triangle-plane triangle1))
            ;(t2_plane (triangle-plane triangle2))
            (t1_lines (triangle-line-list triangle1))
            (t2_lines (triangle-line-list triangle2)))
        (cond ((or (some #'(lambda (line) ; line = (from . delta) 
                             (collision-detection-lay-vector-triangle
                              (make-instance 'lay-vector :position (car line) :lay-to (cdr line))
                              triangle2))
                         t1_lines)
                   (some #'(lambda (line)
                             (collision-detection-lay-vector-triangle
                              (make-instance 'lay-vector :position (car line) :lay-to (cdr line))
                              triangle1))
                         t2_lines))
               (values triangle2 triangle1))
              (t
               (values nil nil)))))))


(defun collision-detection-sphere-sphere (sphere1 sphere2)
  ;; 
  (with-slots ((s1_r radius) (s1_p position)) sphere1
    (with-slots ((s2_r radius) (s2_p position)) sphere2
      (let ((delta_p (vec3-sub s2_p s1_p))
            (sumof_r (+ s2_r s1_r)))
        (if (< (abs (vec3-magnitude^2 delta_p)) (square sumof_r))
            (values sphere2 sphere1)
            (values nil nil))
        ))))



(defun collision-detection-plane-sphere (plane sphere)
  (with-slots ((s_c position) radius) sphere
    (let ((dist (abs (distance-point-plane s_c plane))))
      (if (> radius dist)
          (values sphere plane)
          (values nil nil)))))


(defun collision-detection-polygon-polygon (polygon1 polygon2)
  polygon1 polygon2 nil
  (values nil nil)
)



(defun axis-test (axis-a axis-b length)
  (if (> length (+ axis-a axis-b))
      nil t))

(defun len-seg-on-separate-axis (sep e1 e2 e3)
  ;; length of projected to sepalation axis from each axis component
  (let ((r1 (abs (vec3-dot sep e1)))
        (r2 (abs (vec3-dot sep e2)))
        (r3 (abs (vec3-dot sep e3))))
    (+ r1 r2 r3)))

(defun collision-detection-obb-obb (obb1 obb2)
  (let ((fur-1 (obb-forward-up-right obb1))
        (fur-2 (obb-forward-up-right obb2))
        (whd-1 (vec3-multiply (obb-width-height-depth obb1) 1/2))
        (whd-2 (vec3-multiply (obb-width-height-depth obb2) 1/2))
        (pos1 (slot-value obb1 'position))
        (pos2 (slot-value obb2 'position)))
    (let (; Normal of Ae{1,2,3} , Be{1,2,3}
          ; delta position
          (NAe1 (aref fur-1 0)) (NAe2 (aref fur-1 1)) (NAe3 (aref fur-1 2))
          (NBe1 (aref fur-2 0)) (NBe2 (aref fur-2 1)) (NBe3 (aref fur-2 2))
          (interval (vec3-sub pos1 pos2)))
      (let (; Ae{1,2,3}, Be{1,2,3}
            (Ae1 (vec3-multiply NAe1 (aref whd-1 0))) (Ae2 (vec3-multiply NAe2 (aref whd-1 1)))
            (Ae3 (vec3-multiply NAe3 (aref whd-1 2)))
            (Be1 (vec3-multiply NBe1 (aref whd-2 0))) (Be2 (vec3-multiply NBe2 (aref whd-2 1)))
            (Be3 (vec3-multiply NBe3 (aref whd-2 2)))
            (zerov *zero-vec3*))
        (if
         (and
          ; horizonal axis
          (axis-test (vec3-magnitude Ae1) ;; Axis Ae1
                     (len-seg-on-separate-axis NAe1 Be1 Be2 Be3)
                     (abs (vec3-dot interval NAe1)))
          (axis-test (vec3-magnitude Ae2) ;; Axis Ae2
                     (len-seg-on-separate-axis NAe2 Be1 Be2 Be3)
                     (abs (vec3-dot interval NAe2)))
          (axis-test (vec3-magnitude Ae3) ;; Axis Ae3
                     (len-seg-on-separate-axis NAe3 Be1 Be2 Be3) 
                     (abs (vec3-dot interval NAe3)))
          ;
          (axis-test (len-seg-on-separate-axis NBe1 Ae1 Ae2 Ae3) ;; Axis Be1
                     (vec3-magnitude Be1)
                     (abs (vec3-dot interval NBe1)))
          (axis-test (len-seg-on-separate-axis NBe2 Ae1 Ae2 Ae3) ;; Axis Be2
                     (vec3-magnitude Be2)
                     (abs (vec3-dot interval NBe2)))
          (axis-test (len-seg-on-separate-axis NBe3 Ae1 Ae2 Ae3) ;; Axis Be3
                     (vec3-magnitude Be3)
                     (abs (vec3-dot interval NBe3)))
          ; crossing axis
          ;
          (let ((cross (vec3-cross NAe1 NBe1))) ; Axis C11
            (axis-test (len-seg-on-separate-axis cross Ae2 Ae3 zerov)
                       (len-seg-on-separate-axis cross Be2 Be3 zerov)
                       (abs (vec3-dot interval cross))))
          (let ((cross (vec3-cross NAe1 NBe2))) ; Axis C12
            (axis-test (len-seg-on-separate-axis cross Ae2 Ae3 zerov)
                       (len-seg-on-separate-axis cross Be1 Be3 zerov)
                       (abs (vec3-dot interval cross))))
          (let ((cross (vec3-cross NAe1 NBe3))) ; Axis C13
            (axis-test (len-seg-on-separate-axis cross Ae2 Ae3 zerov)
                       (len-seg-on-separate-axis cross Be1 Be2 zerov)
                       (abs (vec3-dot interval cross))))
          ;
          (let ((cross (vec3-cross NAe2 NBe1))) ; Axis C21
            (axis-test (len-seg-on-separate-axis cross Ae1 Ae3 zerov)
                       (len-seg-on-separate-axis cross Be2 Be3 zerov)
                       (abs (vec3-dot interval cross))))
          (let ((cross (vec3-cross NAe2 NBe2))) ; Axis C22
            (axis-test (len-seg-on-separate-axis cross Ae1 Ae3 zerov)
                       (len-seg-on-separate-axis cross Be1 Be3 zerov)
                       (abs (vec3-dot interval cross))))
          (let ((cross (vec3-cross NAe2 NBe3))) ; Axis C23
            (axis-test (len-seg-on-separate-axis cross Ae1 Ae3 zerov)
                       (len-seg-on-separate-axis cross Be1 Be2 zerov)
                       (abs (vec3-dot interval cross))))
          ;
          (let ((cross (vec3-cross NAe3 NBe1))) ; Axis C31
            (axis-test (len-seg-on-separate-axis cross Ae1 Ae2 zerov)
                       (len-seg-on-separate-axis cross Be2 Be3 zerov)
                       (abs (vec3-dot interval cross))))
          (let ((cross (vec3-cross NAe3 NBe2))) ; Axis C32
            (axis-test (len-seg-on-separate-axis cross Ae1 Ae2 zerov)
                       (len-seg-on-separate-axis cross Be1 Be3 zerov)
                       (abs (vec3-dot interval cross))))
          (let ((cross (vec3-cross NAe3 NBe3))) ; Axis C33
            (axis-test (len-seg-on-separate-axis cross Ae1 Ae2 zerov)
                       (len-seg-on-separate-axis cross Be1 Be2 zerov)
                       (abs (vec3-dot interval cross)))))
         (values obb2 obb1) ; if t
         (values nil nil) ; if nil
         )))))





