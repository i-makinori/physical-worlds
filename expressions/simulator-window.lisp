(in-package :physical-worlds)

;;;; meta-class for simulator-window


;; timer

(defclass gluts-timer (timer-template)
  ()
  )

(defmethod what-time-of ((tt gluts-timer))
  (glut:get :elapsed-time))


;; model :: physical-world model

(defclass model ()
  ((objects-series :initform #()
                   :accessor model-objects)
   (timer :initform (make-instance 'timer-template) ;; physical-timer
          :accessor model-timer)
   (perspector :initform (make-instance 'euler-camera
                                        :focussing (vector-3d 0 0 0)
                                        :distance 6
                                        :euler-xyz-angle (euler-xyz-angle 0 0 0))
               :accessor model-perspector)))


(defmethod update-model ((m model))
)


;;

(defclass simulator-window (glut:window)
  (;; physical-objects
   (model :initform (make-instance 'model) :accessor displays-model)
   ;; timer
   (displays-timer 
    :accessor displays-timer
    ;;:initform (make-instance 'timer-template))
    :initform (make-instance 'gluts-timer))
   ;; inputtting-wrapper
   (input-wrapper :initform (make-instance 'input-wrapper)
                  :accessor input-wrapper)
   )
  (:default-initargs :width 500 :height 500 :pos-x 100 :pos-y 100
   :mode '(:double :rgb :depth) :title "simulator-window"))


(defmethod displays-camera ((w simulator-window))
  (slot-value (slot-value w 'model) 'perspector))

(defmethod init-simulator-window ((w simulator-window))
  (look-from (displays-camera w))
  ;;
  (gl:clear-color 0 0 0 1)
  ;;(gl:shade-model :smooth)
  ;;
  ;(glut:init-display-mode :multisample)
  (gl:disable :depth-test)
  (gl:enable :blend)
  (gl:blend-func :src-alpha :one-minus-src-alpha)
  ;;
  (gl:enable :lighting)
  (gl:enable :light0)
  (gl:enable :depth-test))

(defmethod update-gl-params-when-display ((w simulator-window) camera)
  ;; clear projection matrix stack
  (gl:matrix-mode :projection)
  (gl:load-identity)
  (reperspect! camera (glut:get :window-width) (glut:get :window-height))

  ;; clear model view matrix stack
  (gl:matrix-mode :modelview)
  (gl:load-identity)
  ;;(look camera)
  
  ;; enable modes
  (gl:enable :depth-test :lighting)

  ;; viewport
  (reset-viewport w)

  ;; clear color buffer and depth buffer
  (gl:clear :color-buffer-bit :depth-buffer-bit))


(defmethod reset-viewport ((w simulator-window))
  (let ((width (glut:get :window-width))
        (height (glut:get :window-height))
        (pad 5))
    (if (and (> width (* pad 2)) (> height (* pad 2)))
        (gl:viewport pad pad (- width pad pad) (- height pad pad))
        (gl:viewport 0 0 0 0))  
  ))



(defmethod glut:display ((w simulator-window))
  (progn
    ;(gl:clear :color-buffer :depth-buffer)
    ;; character-rotation

    ;; manipulate model by input
    (update-world-by-input nil w)
    (update-displaying-by-input w)
    ;; (update-world-and-window-by-side_effect_io w)
    

    ;; update model
    ;; (update-model (displays-model w))
    ;;  is called by timer. 
    ()

    ;;
    (progn
      (update-gl-params-when-display w (displays-camera w))
      ;; camera 
      (look (displays-camera w))
      ;; world
      (en-projector-from-model w)
      ;; status
      (draw-status-texts w)
      ;;
      (glut:swap-buffers)
      ;;(gl:flush)
      )
    (reset-for-frame-update---input-wrapper-status (input-wrapper w))
    ))


;;;; reshape handler

(defmethod reshape ((w simulator-window))
  (let ((camera (displays-camera w)))
    (reset-viewport w)
    (reperspect! camera (glut:get :window-width) (glut:get :window-height))
  ))

(defmethod glut:reshape ((w simulator-window) width height)
  (reshape w))

;;;; Mouse input handler

(defmacro mouse-up-down-updation-key (input-wrapper button button-code accesser state x y)
  `(with-slots (,accesser) ,input-wrapper
     (if (eq ,button ,button-code)
         (if (eq ,state :down)
             (setf ,accesser (cons :down (2d-point ,x ,y)))
             (setf ,accesser nil)))))

(defmethod glut:mouse ((w simulator-window) button state x y)
  (with-slots (input-wrapper) w
    (mouse-up-down-updation-key
     input-wrapper button :left-button mouse-left state x y)
    (mouse-up-down-updation-key
     input-wrapper button :middle-button mouse-middle state x y)
    (mouse-up-down-updation-key
     input-wrapper button :right-button mouse-right state x y))
  (redisplay w))


(defun mouse-delta-reset (input-wrapper new-x new-y)
  (with-slots (mouse-xy-current mouse-xy-delta) input-wrapper
    (setf mouse-xy-delta (2d-point-delta (2d-point new-x new-y) mouse-xy-current))
    (setf mouse-xy-current (2d-point new-x new-y))))

(defmethod glut:motion ((w simulator-window) x y)
  (mouse-delta-reset (input-wrapper w) x y)
  (redisplay w))

(defmethod glut:passive-motion ((w simulator-window) x y)
  (mouse-delta-reset (input-wrapper w) x y)
  (redisplay w))

(defmethod glut:mouse-wheel ((w simulator-window) button dir x y)
  button x y
  (with-slots (mouse-wheel) (input-wrapper w)
    (setf mouse-wheel dir))
  (redisplay w))


;;;; keyboard input handler

(defun remove-modifiered-released-keys (keys modifiers-now)
  ;; defect: hash of mofifier and press/releasing key code 
  ;; at (if (or addtion/or/released-modifiers) ...)
  (maphash
   #'(lambda (key val)
       (let* ((modifiers-ago (cadr val))
              (released-modifiers (set-difference modifiers-now modifiers-ago))
              (addtiond-modifiers (set-difference modifiers-ago modifiers-now)))
         ;;(format t "key:~A,~A, R:~A, A:~A~%" key val released-modifiers addtiond-modifiers)
         (if (or addtiond-modifiers released-modifiers) ; defect
             (remhash key keys))))
   keys))

(defun remove-released-key-and-keys (key keys)
  (remhash key keys)
  (remove-modifiered-released-keys keys (glut:get-modifiers)))

(defun add-just-pressed-key-to-keys (key keys)
  (setf (gethash key keys)
        (list :just-pressed (glut:get-modifiers))))


(defmethod glut:special-up ((w simulator-window) key x y)
  (declare (ignore x y))
  ;;
  (with-slots (keys) (input-wrapper w)
    (remove-released-key-and-keys key keys))
  ;;  
  (redisplay w))

(defmethod glut:keyboard-up ((w simulator-window) key x y)
  (declare (ignore x y))
  ;;
  (with-slots (keys) (input-wrapper w)
    (remove-released-key-and-keys key keys))
  ;;
  (redisplay w))


(defmethod glut:special ((w simulator-window) key x y)
  (declare (ignore x y))
  ;;
  (with-slots (keys) (input-wrapper w)
    (add-just-pressed-key-to-keys key keys))
  ;;
  (redisplay w))

(defmethod glut:keyboard ((w simulator-window) key x y)
  (declare (ignore x y))
  ;;
  (with-slots (keys) (input-wrapper w)
    (add-just-pressed-key-to-keys key keys))
  ;;
  (if (eql key #\Esc) 
      (exit-window w)
      (redisplay w)))
