(in-package :physical-worlds)

;;

(defun aref-or-nil (array &rest subscripts)
  (handler-case 
      (apply #'aref array subscripts)
    (SB-INT:INVALID-ARRAY-INDEX-ERROR (c)
      c nil)))

(defun forcus-aref (object-array &rest subscripts)
  (let ((obj-maybely
          (apply #'aref-or-nil object-array subscripts)))
    (if (null obj-maybely) *zero-vec3* obj-maybely
  
  )))


;;


(defparameter *num-plane* 0)
(defparameter *num-spheres* 2)
(defparameter *num-lays* 2)
(defparameter *num-triangles* 2)
(defparameter *num-obbs* 3)

;;

(defun spheres-rotation-where_d (omega time d_theta)
  (vector-3d 
   ;(* 0.0001 (sin (+ d_theta (* omega time))))
   0
   (* 0.03 (cos (+ d_theta (* omega time))))
   (* 0.01 (sin (+ d_theta (* omega time))))
             ))

(defun spheres-rotation-where! (sphere iter len d_time)
  (with-slots (position velocity) sphere
    (setf velocity
          (spheres-rotation-where_d
           (* (+ 1.00 (* iter 0.02)) (/ (+ iter 1) len))
           d_time
           0))
    (update-object sphere)
    sphere))

(defun lay-vectors-rotation-where_d (omega time d_theta)
  (vector-3d 0
             (* 5 (cos (+ d_theta (* omega time))))
             (* 2 (sin (+ d_theta (* omega time))))))

(defun lay-vectors-rotation-where! (lay iter len d_time)
  iter len d_time nil lay
  (with-slots (position lay-to) lay
    (setf position
          (lay-vectors-rotation-where_d
           (* (+ 1.00 (* iter 0.01)) (/ (+ iter 1) len))
           d_time
           0))
    (setf lay-to
          (vec3-multiply (vector-3d (sin (+ 0 (* d_time (* iter 0.1))))
                                    (cos (+ 0 (* d_time (* iter 0.1))))
                                    0)
                         3))
    lay))

(defun triangle-rotation-where-d (omega time d_theta)
  (vector-3d 0
             (* 4 (cos (+ d_theta (* omega time))))
             (* 2 (sin (+ d_theta (* omega time))))))

(defun triangle-rotation-where! (triangle iter len d_time)
  (with-slots (position position2 position3) triangle
    (let ((p_s (triangle-rotation-where-d
                (* (+ 1.00 (* (1+ iter) 0.01)) (/ (* iter 1) len))
                d_time
                0))
          (rotate1 
            (vector-3d (* 2/1 (sin (+ iter (* d_time 0.1))))
                       (* 2/1 (cos (+ iter (* d_time 0.1))))
                       0)))
      (setf position  (vec3-add p_s (vector-3d 0 0 -2)))
      ;(setf position2 (vec3-add p_s (vector-3d -4 0 5)))
      ;(setf position3 (vec3-add p_s (vector-3d -0.2 0 5)))
      (setf position2 (vec3-add p_s rotate1))
      (setf position3 (vec3-add p_s (vec3-reverse rotate1)))
      (triangle-normal! triangle)
      triangle)))

(defun obb-rotation-where-d (omega time d_theta)
  (vector-3d 0
             (* 0.04 (cos (+ d_theta (* omega time))))
             (* 0.02 (sin (+ d_theta (* omega time)))))
  )

(defun obb-rotations-where! (obb iter len d_time)
  (with-slots (position velocity rotation rotation-velocity) obb
    (let ((p_s (obb-rotation-where-d
                (* (+ 1.00 (* (1+ iter) 0.2)) (/ (* iter 1) (1+ len)))
                d_time
                0))
          (rotate1
            (vector-3d (* 0.1 (sin iter)) ;(* 2 (sin (+ iter))) ;(* d_time 0.02))))
                       (* 0.1 (cos iter)) ;(* 2 (cos (+ iter))) ;(* d_time 0.02))))
                       (* 0.1 (cos (+ iter 0.5))))))
      (setf velocity p_s)
      (setf rotation-velocity 
            (rotation-quaternion-of-rodrigues 0.01 rotate1))
            ;(hamilton-product-matrix 
            ;(rotation-quaternion-by-vec3 0.1 rotate1)))
      ;(rotation-velocity
      (update-object obb)
      obb
    )))


(defun init-objects (plane-len triangle-len sphere-len lays-len obbs-len)
  (coerce
   (append
    ;; plane
    (loop for i from 0 to (1- plane-len)
          collect (make-instance 'plane
                                 :normal (vector-3d (- (random 0.1) 0.05) 0.5 (- (random 0.1) 0.05))
                                 :position (vector-3d 0.0 (- (random 4.0) 2.0) 0.0)))
    ;; triangle
    (loop for i from 0 to (1- triangle-len)
          collect (triangle-rotation-where! (make-instance 'triangle) i triangle-len 0))
    ;; sphere
    (loop for i from 0 to (1- sphere-len)
          collect (spheres-rotation-where!
                   (make-instance 'sphere :radius (+ 0.3 (* 0.2 (random 1.00)))) i sphere-len 0))
    ;; lays
    (loop for i from 0 to (1- lays-len)
          collect (lay-vectors-rotation-where! (make-instance 'lay-vector) i lays-len 0))
    ;; obbs
    (loop for i from 0 to (1- obbs-len)
          collect (obb-rotations-where! 
                   (make-instance 'obb
                                  :width-height-depth `#(1 ,(+ 1 i) ,(+ 0.5 (/ i 2))))
                                  i lays-len 0)))
   'vector))


(defun update-objects (objects current-time)
  (let* ((len (length objects)))
    (loop for i from 0 to (1- len)
          do (let ((obj (aref objects i)))
               (when (typep obj 'plane)
                 nil)
               (when (typep obj 'triangle)
                 (triangle-rotation-where!
                  obj i len current-time))
               (when (typep obj 'lay-vector)
                 (lay-vectors-rotation-where!
                  obj i len current-time))
               (when (typep obj 'sphere)
                 (spheres-rotation-where!
                  obj i len current-time))
               (when (typep obj 'obb)
                 (obb-rotations-where!
                  obj i len current-time))))))

;;

(defclass collision-detect-model (model) 
  ((objects-series 
    :accessor objects-series
    :initform (init-objects *num-plane* *num-triangles* *num-spheres* *num-lays* *num-obbs*))
   (collisioning-series
    :accessor collisioning-series
    :initform (make-array (list (+ *num-plane* *num-triangles* *num-spheres* *num-lays* *num-obbs*))
                          :initial-element '()))
   (focussing-sphere 
    :accessor focussing-sphere
    :initform (make-instance 'sphere
                             :position (vector-3d 0.5 0.5 0.5)
                             :radius 0.1))))


;;;


(defclass test-collisions-window (simulator-window)
  ((model :initform (make-instance 'collision-detect-model))
   )
   (:default-initargs :title "collision test window"))

(defmacro object-series (simulator-window)
  `(model-objects (displays-model ,simulator-window)
                  ))


(defmethod glut:display-window :before ((w test-collisions-window))
  (with-slots (focussing euler-xyz-angle) (displays-camera w)
    (setf focussing (slot-value (displays-model w) 'focussing-sphere))
    (setf euler-xyz-angle (vector-3d 0 (* -7/16 *pi*) (* 1/8 *pi*))))
  (init-simulator-window w))

(defmethod reset-collisioning-array! ((m collision-detect-model))
  (with-slots (collisioning-series) m
    (setf collisioning-series
          (make-array (list (length collisioning-series)) :initial-element '()))))


(defmethod update-model ((m collision-detect-model))
  ;COLLISION-DETECT-MODEL
  (setf (collisioning-series m) (reset-collisioning-array! m))
  (with-slots ((current-time current-time-step)) (slot-value m 'timer)
    (let ((objects (objects-series m)))
      (update-objects objects current-time)
      (setf (collisioning-series m)
            (collision-detection-whole-each! objects (collisioning-series m)))
      )
    (setf current-time (+ current-time 0.01))))

(defmethod draw-status-texts ((w test-collisions-window))
  (gl-color *color-white*)
  (let ((time (slot-value (displays-timer w) 'frame-counter)))
    (draw-string-2d (format-timer-template-status (displays-timer w))
                    0.1 0.1)
    time))


;;

(defmethod update-model-by-input ((w test-collisions-window))
  w)

(defmethod update-displaying-by-input ((w test-collisions-window))
  (let ((wrapper (input-wrapper w))
        (camera (displays-camera w))
        (width (glut:get :window-width))
        (height (glut:get :window-height)))
    (with-slots (mouse-wheel) wrapper
      (if (eql mouse-wheel :up)         (fovy-zoom camera +1.00))
      (if (eql mouse-wheel :wheel-down) (fovy-zoom camera -1.00)))
    (manipulate-camera-wraparound-by-mouse-move camera wrapper width height 32 
                                                'mouse-right 'mouse-middle)
    (let ((focuss (slot-value (displays-model w) 'focussing-sphere)))
      (manipulate-obj-move-relate-camera-by-key focuss wrapper 0.02 camera
                                                #\s #\w #\z #\e #\a #\d))
    ;; time step
    (with-slots (keys) wrapper
      (with-slots (configured-time-step) (displays-timer w)
        (if (key-just-pressed? #\p keys)
            (switch-call-or-stop-timer (displays-timer w))) ;; pause and restart
        (if (key-pressing? #\o keys) (setf configured-time-step (* configured-time-step (/ 1 1.05))))
        (if (key-pressing? #\i keys) (setf configured-time-step (* configured-time-step (* 1 1.05))))))
      ;
  w))

(defmethod en-projector-from-model ((w test-collisions-window))
  (gl:with-pushed-matrix
    (let ((objects (object-series w))
          (collisionings (collisioning-series (displays-model w))))
      collisionings
      (loop
        for i from 0 to (1- (length objects))
        ;do (draw-string-2d (format nil "~A" (length collisionings)) 0.9 0.8)
        do (gl-color *color-white*)
           (let* ((the-object (aref objects i))
                  (pos (obj-position the-object))
                  (collisioning (aref collisionings i)))
             (cond (collisioning
                    (gl-color *color-yellow*)
                    (projection-wire the-object)
                    (draw-string (format nil "COL: ~A" collisioning)
                                 (vec3-s1 pos) (+ (vec3-s2 pos) 0.5) (vec3-s3 pos)))
                   (t
                    ;(gl-color *color-white*)
                    (gl:material :front :ambient #(0.5 0.5 0.8 0.2))
                    (projection the-object)))
             (if collisioning
                 (draw-string
                  (format nil "~A" the-object)
                  (vec3-s1 pos) (+ (vec3-s2 pos) 0.8) (vec3-s3 pos)))
             (when (typep the-object 'triangle)
               (with-slots (position >normal) the-object
                 (project-vector position (vec3-normalize >normal))))
             )))

    (let ((focuss (slot-value (displays-model w) 'focussing-sphere)))
      (projection-wire focuss))
    (project-xyz-axis)
    ;(project-vector *zero-vec3* *base-vec3-axis-x*)
  w))

;;


(defun test-of-collisions ()
  (let ((window (make-instance 'test-collisions-window)))
    ;; timer
    (timer-s-params-set window 0)
    (call-timer window 0)
    ;; key
    (glut:set-key-repeat :key-repeat-off)
    ;;
    (glut:display-window window)
  ))
