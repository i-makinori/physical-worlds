
(in-package :physical-worlds)


;;;;; boundary domain objecs
;;; 境界や領域に或る元の単位の定義の集合

(defclass domain-object ()
  (;; position
   (position ;; position of origin point in the world Axis system.
    :initarg :position :accessor obj-position
    :initform (vector-3d 0 0 0))
   (velocity
    :initarg :velocity :accessor obj-velocity
    :initform (vector-3d 0 0 0))
   ;; for rigid
   (rigid-p ;; is this rigid physical object?
    :initarg :mass :accessor rigid-p
    :initform nil)
   (acceleration ;; a, acceleration vector
    :initarg :acceleration :accessor obj-acceleration
    :initform (vector-3d 0 0 0))
   (mass
    :initarg :mass :accessor mass
    :initform 1)
   (virtual-radius
    :initarg :virtual-radius :accessor virtual-radius
    :initform 1)
   ;; rotation
   (rotation ;; rotation quaternion
    :initarg :rotation :accessor obj-rotation
    :initform (rotation-quaternion-ss 0 1 0 0))
   (rotation-velocity ;; Omega
    :initarg :rotation-velocity :accessor obj-rotation-velocity
    :initform (rotation-quaternion-ss 0 1 0 0))
   ;; values
   (scale 
    :initarg :scale
    :initform (vector-3d 1 1 1))
   (constituent
    ;; constituent of this object. 
    ;; if nil, this is primitive object. if list, synthesized object.
    :initarg :constituency :accessor obj-constituency
    :initform nil)))


;(defclass local-object ()
;  (vertex-array)
;  (polygon-array)
;  (normal-array)
;  (texchar-array)
;  (>vertex-at-world))


(defparameter *zero-domain*
  (make-instance 'domain-object))

(defmethod displacement ((obj domain-object))
  ;; (delta / delta_time) obj
)

(defmethod projection ((obj domain-object))
  ;; project to physical(dimension is almost 3 or more) world.
  
)

(defmethod object-primitivep ((obj domain-object))
  (null (obj-constituency obj)))

(defmethod object-synthesizedp ((obj domain-object))
  (listp (obj-constituency obj)))

;;

(defclass rigid (domain-object)
  ())

;;


(defmethod object-forward-up-right ((obj domain-object))
  #|
  (let ((x-base *base-vec3-axis-x*)
        (y-base *base-vec3-axis-y*)
        (z-base *base-vec3-axis-z*))
    (with-slots (rotation) obj
      (matrix-multiplication
       (rotation-quaternion-rotate-point rotation)
       (deploy-vec3-to-conversion-products (list x-base y-base z-base)))))
  |#
  `#(,(object-forward obj) ,(object-up obj) ,(object-right obj)))


(defmethod object-forward ((obj domain-object))
  (let ((base-vec *base-vec3-axis-x*))
    (with-slots (rotation) obj
      (vec3-normalize
       (vec4-sss0-vec3
        (matrix-vector-multiplication
         (rotation-quaternion-rotate-point rotation)
         (vec3-to-vec4 base-vec)))))))

(defmethod object-up ((obj domain-object))
  (let ((base-vec *base-vec3-axis-y*))
    (with-slots (rotation) obj
      (vec3-normalize
       (vec4-sss0-vec3
        (matrix-vector-multiplication
         (rotation-quaternion-rotate-point rotation)
         (vec3-to-vec4 base-vec)))))))

(defmethod object-right ((obj domain-object))
  (let ((base-vec *base-vec3-axis-z*))
    (with-slots (rotation) obj
      (vec3-normalize
       (vec4-sss0-vec3
        (matrix-vector-multiplication
         (rotation-quaternion-rotate-point rotation)
         (vec3-to-vec4 base-vec)))))))

;;

(defparameter *time-step* 0.02
  ;; delta_t is (0.02sec === 1/50 sec)
  )

(defmethod move-object! ((obj domain-object) d_position)
  (with-slots (position) obj
    (setf position (vec3-add position d_position))
    obj))

(defmethod update-velocity ((obj domain-object))
)

(defmethod rotate-object! ((obj domain-object) d_rq)
  ;; rotate for position-system
  ;; q_n = q_1 q_2 ... q_n-1
  (when (rotation-quaternionp d_rq)
    (with-slots (rotation rotation-velocity) obj
      ;;(print rotation)
      (setf rotation (rotation-quaternion (quaternion-product rotation d_rq)))
))
  obj)

(defmethod update-object ((obj domain-object))
  ;; next 
  (with-slots (position velocity) obj
    (move-object! obj velocity)
    )
  ;; next rotation
  (with-slots (rotation rotation-velocity) obj
    (rotate-object! obj rotation-velocity)
    )
  obj
)


(defmethod update-object-physically ((obj domain-object))
  ;; next Position
  (with-slots (position velocity acceleration) obj
    ; v(t+dt) = v(t) + a(t)*delta_t.
    (setf velocity (vec3-add velocity (vec3-multiply acceleration *time-step*))) ; next velocity
    ; R(t+dt) = R(t) + v(t)*delta_t. , where R is position.
    (move-object! obj (vec3-multiply velocity *time-step*))) ; next position

  ;; next rotation
  (with-slots (rotation rotation-velocity) obj
    ;;(rotate-object! obj (vec3-multiply rotation-velocity *time-step*)) ; next rotation (?)
    (rotate-object! obj (rotation-quaternion (quaternion-multiply rotation-velocity *time-step*))) ; next rotation (?)
    )
  ;;(print (obj-position obj))
  obj
)

;;; physics for point-mass



(defmethod add-force-for-permanent! ((obj domain-object) force)
  "force is 3d-vector. for all times"
  ;; because, update acceleration
  ;; F(t) = m * a <=> a = F * (1 / m)
  (with-slots (acceleration mass) obj
    (progn
      (setf acceleration
            (vec3-add acceleration 
                      (vec3-multiply force (/ 1 mass))))
      (values obj acceleration))))

(defmethod add-force-for-time-step! ((obj domain-object) force)
  "force is 3d-vector. for once time-step"
  ;; because, update velocity
  ;; force of argument is force_of_outer.
  ;; F(t) = m * a <=> a = F * (1 / m)
  ;; V(t) = A_inner + A_outer*delta_t
  ;;      = A_inner + F*(1/m)*delta_t
  ;; but, A_inner force is added with update-object function called
  ;; V(t) = V(t) + A_outer*delta_t
  (with-slots (velocity mass) obj
    (progn
      (setf velocity
            (vec3-add velocity
                      (vec3-multiply force (* (/ 1 mass) *time-step*))))
      ;;(format t "velocity is ~A ~%" velocity)
      velocity)))



;;; rotation



(defmethod enroll ((obj domain-object) theta-x)
  (rotate-object! obj
                  (rotation-quaternion-of-rodrigues theta-x *base-vec3-axis-x*))
  obj)

(defmethod enpitch ((obj domain-object) theta-y)
  (rotate-object! obj
                  (rotation-quaternion-of-rodrigues theta-y *base-vec3-axis-y*))
  obj)

(defmethod enyaw ((obj domain-object) theta-z)
  (rotate-object! obj
                  (rotation-quaternion-of-rodrigues theta-z *base-vec3-axis-z*))
  obj)




;;

(defmethod object-model-world-gl-conv ((obj domain-object) &key (no-scale nil))
  (with-slots (scale rotation position) obj
    (gl-model-world-conversion :scale (if no-scale (vector-3d 1 1 1) scale)
                               :rotation rotation
                               :translate position)))

(defun project-rotaion-axis (rq &key (scale 0.50))
  (multiple-value-bind (rad axel) (rotation-quaternion-s-angle-axis rq)
    rad
    (let* ((axel-from (vec3-multiply axel (- scale)))
           (axel-to   (vec3-multiply axel (* 2 scale))))
    (project-vector axel-from
                    axel-to
                    ))))

(defmethod projection-slots-params ((obj domain-object)
                                    &key (rotation-axis nil) (xyz-axis nil) (scale 0.5))
  (with-slots (rotation) obj
    (gl:with-pushed-matrix
      (object-model-world-gl-conv obj :no-scale t)
      (when rotation-axis
        (project-rotaion-axis rotation :scale scale))
      (when xyz-axis
        (project-xyz-axis :scale scale)
        obj))))

;;


(defclass point (domain-object)
  ())

(defmethod projection ((p point))
  ()
)




;;; lay vector

(defclass lay-vector (domain-object)
  ((lay-to
    :initarg :lay-to :accessor lay-to
    :initform (vector-3d 0 0 0))))

(defmethod projection ((lay lay-vector))
  (with-slots (position lay-to) lay
    (project-vector position lay-to)))

(defmethod projection-wire ((lay lay-vector))
  (projection lay))

;;; plane

(defclass plane (domain-object)
  ;; waypoint(as position) and normal vector
  ((normal
    :initarg :normal :accessor plane-normal
    :initform (vector-3d 0 1 0))
   (>tangent1 :initform (vector-3d 1 0 0))
   (>tangent2 :initform (vector-3d 0 0 1))))

(defmethod plane-tangents! ((plane plane))
  ;; normalize normal vector, and get (main and subord) tanget vectors
  (with-slots (normal >tangent1 >tangent2) plane
    (setf normal (vec3-normalize normal))
    (let ((nx (vec3-s1 normal))
          (ny (vec3-s2 normal))
          (nz (vec3-s3 normal)))
      (let ((tangent1
              (cond ((not (~= nz 0)) (vector-3d 0 1 (- (/ ny nz))))
                    ((not (~= ny 0)) (vector-3d 1 (- (/ nx ny)) 0))
                    ((not (~= nx 0)) (vector-3d (- (/ nz nx)) 0 1))
                    (t (warn "plane has no normal vector") nil))))
        (when tangent1
          (setf >tangent1 (vec3-normalize tangent1))
          (setf >tangent2 (vec3-normalize (vec3-reverse (vec3-cross tangent1 normal))))
          (values >tangent1 >tangent2))))))

(defun projection-plane! (plane &key (virtual-width 10))
  (plane-tangents! plane)
  (with-slots (position normal >tangent1 >tangent2) plane
    (let*
        ((v-width (/ virtual-width 2))
         (v1 (vec3-multiply (vec3-add (vec3-reverse >tangent1) (vec3-reverse >tangent2)) v-width))
         (v2 (vec3-multiply (vec3-add (identity >tangent1)     (vec3-reverse >tangent2)) v-width))
         (v3 (vec3-multiply (vec3-add (identity >tangent1)     (identity >tangent2))     v-width))
         (v4 (vec3-multiply (vec3-add (vec3-reverse >tangent1) (identity >tangent2))     v-width)))
      (gl:with-pushed-matrix
        (gl-translate-by-vec3 position)
        (gl:begin :quads)
        (gl-vertex-by-vec3 v1)
        (gl-vertex-by-vec3 v2)
        (gl-vertex-by-vec3 v3)
        (gl-vertex-by-vec3 v4)
        (gl:end)))))

(defmethod projection ((plane plane))
  (projection-plane! plane :virtual-width 10))


(defun projection-plane-wire! (plane &key (num-lines 10) (virtual-width 10))
  (plane-tangents! plane)
  (with-slots (position normal (u1 >tangent1) (v1 >tangent2)) plane
    (let ((u1-abs-edge (vec3-multiply u1 (* +1/2 virtual-width)))
          (v1-abs-edge (vec3-multiply v1 (* +1/2 virtual-width))))
      (gl:with-pushed-matrix
        (gl:disable :lighting)
        (gl-translate-by-vec3 position)
        ;(gl:translate 0 0.1 0)
        (loop for i from 0 to num-lines
              do (let* ((v1-i (vec3-multiply v1 
                                             (* (- i (/ num-lines 2)) virtual-width (/ num-lines)))))
                   (gl:begin :lines)
                   (gl-vertex-by-vec3 (vec3-add v1-i u1-abs-edge)) ; point+
                   (gl-vertex-by-vec3 (vec3-sub v1-i u1-abs-edge)) ; point-
                   (gl:end)))
        (loop for j from 0 to num-lines
              do (let* ((u1-i (vec3-multiply u1 
                                             (* (- j (/ num-lines 2)) virtual-width (/ num-lines)))))
                   (gl:begin :lines)
                   (gl-vertex-by-vec3 (vec3-add u1-i v1-abs-edge)) ; point+
                   (gl-vertex-by-vec3 (vec3-sub u1-i v1-abs-edge)) ; point-
                   (gl:end)))
        (gl:enable :lighting)))))

(defmethod projection-wire ((plane plane))
  (projection-plane-wire! plane :num-lines 10 :virtual-width 10))


;;; distance

(defun distance (vec3-point1 vec3-point2)
  (vec3-magnitude (vec3-sub vec3-point2 vec3-point1)))

(defun distance-point-plane (vec3-coordinate plane)
  (with-slots (position normal) plane
    (/ (vec3-dot normal (vec3-sub vec3-coordinate position))
       (vec3-magnitude normal))))

(defun nearist-point-of-straght-line (point line-point velocity)
  ;; q = f + t * v,
  ;; Vec(v) dot Vec(qp) = 0,
  ;; ?- t
  ;; t = v dot (p - f) / (v dot v)
  (let* ((v-dot-v (vec3-dot velocity velocity))
         (v-dot-p--f (vec3-dot velocity (vec3-sub point line-point)))
         (time (/ v-dot-p--f v-dot-v)))
    time
    (vec3-add line-point (vec3-multiply velocity time))))

(defun distance-point-line (point line-from line-delta &key (if-zero nil))
  ;; http://marupeke296.com/COL_Basic_No2_ShortTec.html
  ;; (p1) (from)---(p2)------>(from+delta) (p3)
  ;; returns distance of nearist point
  ;; returns nearist point
  (let ((from-to-point (vec3-sub point line-from))
        (len-line-delta (vec3-magnitude line-delta)))
    (cond ((zerop len-line-delta)
           if-zero)
          ((> 0 (vec3-dot line-delta from-to-point)) ;; p1: obtuse and *
           (values (distance point line-from)
                   line-from))
          ((> 0 (vec3-dot line-delta (vec3-sub line-delta from-to-point))) ;; p3: acute and obtuse
           (values (distance point (vec3-add line-from line-delta))
                   (vec3-add line-from line-delta)))
          (t ;; p4: acute and acute
           (values
            (/ (vec3-magnitude (vec3-cross line-delta from-to-point)) len-line-delta)
            (nearist-point-of-straght-line point line-from line-delta))))))


;;; triangle polygon

(defclass triangle (domain-object)
  (; position
   (position2 :initarg position2 :initform (vector-3d 1 0 0) :accessor triangle-position2)
   (position3 :initarg position3 :initform (vector-3d 0 1 0) :accessor triangle-position3)
   (>normal :initform (vector-3d 0 0 1) :accessor triangle->normal)))

(defmethod triangle-normal! ((obj triangle))
  (with-slots (>normal position position2 position3) obj
    ; normal = cross (v1-v0) (v2-v0). where v0 is relatively zero vector (origin pointting).
    (setf >normal (vec3-cross (vec3-sub position2 position) (vec3-sub position3 position)))))

(defmethod triangle-inner-plane ((obj triangle))
  (with-slots (>normal position) obj
    (make-instance 'plane :normal >normal :position position)))


(defmethod triangle-line-list ((obj triangle))
  (with-slots ((p1 position) (p2 position2) (p3 position3)) obj
    (list
     (cons p1 (vec3-sub p2 p1))
     (cons p2 (vec3-sub p3 p2))
     (cons p3 (vec3-sub p1 p3)))))

(defmethod triangle-distance-to-wire ((obj triangle) point)
  (apply #'min
         (remove nil
                 (mapcar #'(lambda (line)
                             (multiple-value-bind (distance point)
                                 (distance-point-line point (car line) (cdr line))
                               point distance))
                         (triangle-line-list obj)))))


(defmethod projection ((obj triangle))
  (triangle-normal! obj)
  (with-slots (position position2 position3 >normal) obj
    (gl:with-pushed-matrix
      ;(gl-translate-by-vec3 position)
      ;(gl:material :front-and-back :ambient-and-diffuse (color-vector 0.8 0.8 0.8))
      
      (gl:begin :triangles)
      ;(gl:begin :polygon)
      ;;
      (gl-normal-by-vec3 >normal)
      (gl-vertex-by-vec3 position)
      (gl-vertex-by-vec3 position2)
      (gl-vertex-by-vec3 position3)
      (gl:end))))


(defmethod projection-wire ((obj triangle))
  (with-slots (position position2 position3) obj
    (gl:with-pushed-matrix
      (gl:disable :lighting)
      (gl:begin :line-strip)
      (gl-vertex-by-vec3 position)
      (gl-vertex-by-vec3 position2)
      (gl-vertex-by-vec3 position3)
      (gl-vertex-by-vec3 position)
      (gl:end)
      (gl:enable :lighting))))


;;; sphere

(defclass sphere (domain-object)
  ;; domain: x^2 + y^2 + z^2 <= 0
  ((radius
    :initarg :radius :accessor sphere-radius
    :initform 1.00)))

(defmethod projection ((s sphere))
  (with-slots (position rotation) s
    (gl:with-pushed-matrix
      (object-model-world-gl-conv s)
      (gl:with-pushed-matrix
        (gl:rotate 90 0 1 0)
        (glut:solid-sphere (sphere-radius s) 10 10)))))

(defmethod projection-wire ((s sphere))
  (with-slots (posision rotation) s
    (gl:with-pushed-matrix
      (gl:disable :lighting)
      (object-model-world-gl-conv s)
      (gl:rotate 90 0 1 0)
      ;;(gl-color *color-cyan*)
      (glut:wire-sphere (sphere-radius s) 10 10)
      (gl:enable :lighting))))



;;; object oriented bounding box

(defclass obb (domain-object) ;; object oriented bounding box
  (;; position is position
   ;; {forward, up, right} from rotation
   (width-height-depth :initarg :width-height-depth :accessor obb-width-height-depth
                       :initform (vector-3d 1 1 1)))
  )

;
(defmethod obb-position ((obb obb)) (obj-position obb))
(defmethod obb-rotation ((obb obb)) (obj-rotation obb))
;
(defmethod obb-forward-up-right ((obb obb)) (object-forward-up-right obb))
(defmethod obb-forward ((obb obb)) (object-forward obb))
(defmethod obb-up ((obb obb))      (object-up obb))
(defmethod obb-right ((obb obb))   (object-right obb))
;
(defmethod obb-width ((obb obb))  (aref (obb-width-height-depth obb) 0))
(defmethod obb-height ((obb obb)) (aref (obb-width-height-depth obb) 1))
(defmethod obb-depth ((obb obb))  (aref (obb-width-height-depth obb) 2))


(defmethod obb-vertex-list ((obb obb))
  (let* ((dim_x+ (vec3-multiply (obb-forward obb) (* 1/2 (obb-width obb))))
         (dim_y+ (vec3-multiply (obb-up obb) (* 1/2 (obb-height obb))))
         (dim_z+ (vec3-multiply (obb-right obb) (* 1/2 (obb-depth obb))))
         ;
         (vertex-list
           (map '(vector * 8) #'(lambda (xyz)
                            (reduce 
                             #'vec3-add
                             `#(,(vec3-multiply dim_x+ (nth 0 xyz))
                                ,(vec3-multiply dim_y+ (nth 1 xyz))
                                ,(vec3-multiply dim_z+ (nth 2 xyz)))))
                   '((+1 +1 +1) (+1 +1 -1) (+1 -1 +1) (+1 -1 -1)
                     (-1 +1 +1) (-1 +1 -1) (-1 -1 +1) (-1 -1 -1)))))
    vertex-list))




(defparameter *obb-vertex-indexes*
  ;;(defmethod obb-vertex-indexes ((obb obb))
  ;; ref: order of obb-vertex-list
  ;; counter clockwise.
  #2A((0 2 3 1) (4 6 2 0) (5 7 6 4) (1 3 7 5) ;; round side
    (0 1 5 4) (7 3 2 6) ;; top and bottom
    ))


    

(defmethod projection ((obj obb))
  (with-slots (rotation position) obj
    (let ((vertex-list (obb-vertex-list obj)))
      (gl:with-pushed-matrix
        (gl-translate-by-vec3 position)
        ;(gl:material :front-and-back :diffuse (color-vector 0.8 0.8 0.8 0.1))
        (gl:with-pushed-matrix
          (loop for i from 0 to (1- (array-dimension *obb-vertex-indexes* 0))        
                do (let ((v1 (aref vertex-list (aref *obb-vertex-indexes* i 0)))
                         (v2 (aref vertex-list (aref *obb-vertex-indexes* i 1)))
                         (v3 (aref vertex-list (aref *obb-vertex-indexes* i 2)))
                         (v4 (aref vertex-list (aref *obb-vertex-indexes* i 3))))
                     (gl:with-primitives :quads
                       (gl-normal-by-vec3 (normal-by-3-vec3 v1 v2 v3))
                       (gl-vertex-by-vec3 v1)
                       (gl-vertex-by-vec3 v2)
                       (gl-vertex-by-vec3 v3)
                       (gl-vertex-by-vec3 v4)))))))))


(defmethod projection-wire ((obj obb))
  (with-slots (position) obj
    (let ((vertex-list (obb-vertex-list obj)))
      (gl:with-pushed-matrix
        (gl-translate-by-vec3 position)
        (gl:disable :lighting)
        (gl:with-pushed-matrix
          (loop for i from 0 to (1- (array-dimension *obb-vertex-indexes* 0))        
                do (let ((v1 (aref vertex-list (aref *obb-vertex-indexes* i 0)))
                         (v2 (aref vertex-list (aref *obb-vertex-indexes* i 1)))
                         (v3 (aref vertex-list (aref *obb-vertex-indexes* i 2)))
                         (v4 (aref vertex-list (aref *obb-vertex-indexes* i 3))))
                     (gl:with-primitives :line-strip
                       (gl-normal-by-vec3 (normal-by-3-vec3 v1 v2 v3))
                       (gl-vertex-by-vec3 v1)
                       (gl-vertex-by-vec3 v2)
                       (gl-vertex-by-vec3 v3)
                       (gl-vertex-by-vec3 v4)
                       (gl-vertex-by-vec3 v1)))))
        (gl:enable :lighting)))))


;;;


(defclass xyz-axis (domain-object)
  ((unit
    :initarg :unit
    :initform 1.00
  )))

;;;




;;

