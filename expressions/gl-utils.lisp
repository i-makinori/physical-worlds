(in-package :physical-worlds)

;; function

(defun exit-window (window)
  window
  (glut:leave-main-loop)
  (glut:destroy-window (glut:get-window))
  ;;(glut:destroy-current-window)
  )

(defmethod redisplay ((w glut:window))
  (if (zerop (glut:get-window))
      (progn (warn "no glut-window there")
             (exit-window w))
      (glut:post-redisplay)))


(defun gl-translate-by-vec3 (vec3)
  (gl:translate (vec3-s1 vec3) (vec3-s2 vec3) (vec3-s3 vec3)))

(defun gl-scale-by-vec3 (vec3)
  (gl:scale (vec3-s1 vec3) (vec3-s2 vec3) (vec3-s3 vec3)))

(defun gl-vertex-by-vec3 (vec3)  
  (gl:vertex (vec3-s1 vec3) (vec3-s2 vec3) (vec3-s3 vec3)))

(defun gl-normal-by-vec3 (vec3)
  (gl:normal (vec3-s1 vec3) (vec3-s2 vec3) (vec3-s3 vec3)))
(declaim (inline gl-normal-by-vec3))


(defun gl-rotate-by-rotation-quaternion (rq)
  (multiple-value-bind (angle axis) (rotation-quaternion-s-angle-axis rq)
    (cond ((complexp angle)
           nil)
          ((numberp angle)
           ;;(project-vector #(0 0 0) (vec3-multiply axis angle))
           (gl:rotate (radian-to-degree angle) (vec3-s1 axis) (vec3-s2 axis) (vec3-s3 axis)))
          (t nil))))


(defun gl-model-world-conversion (&key (scale (vector-3d 1 1 1))
                                    (rotation (rotation-quaternion-ss 0 1 0 0))
                                    (translate (vector-3d 0 0 0)))
  ;;(gl:with-pushed-matrix (this-func ... ) &body) is needed
  (gl-translate-by-vec3 translate)
  (gl-rotate-by-rotation-quaternion rotation)
  (gl-scale-by-vec3 scale)
  )

(defun normal-by-3-vec3 (vec-in vec-center vec-out)
  ;; 
  (vec3-normalize (vec3-cross (vec3-sub vec-out vec-center) (vec3-sub vec-in vec-center))))

(defun gl-primitive-triangle (coord1 coord2 coord3)
  (gl:with-primitives :triangles ; :polygon ; :triangles
    (gl-normal-by-vec3 (normal-by-3-vec3 coord1 coord2 coord3))
    (gl-vertex-by-vec3 coord1) (gl-vertex-by-vec3 coord2) (gl-vertex-by-vec3 coord3)))


;; color

(defun color-vector (r g b &optional (a 1.00))
  `#(,(float r) ,(float g) ,(float b) ,(float a)))

(defun gl-color (color-vector)
  (gl:color (aref color-vector 0) (aref color-vector 1) (aref color-vector 2) (aref color-vector 3)))

(defparameter *color-white* (color-vector 1 1 1 1))
(defparameter *color-red* (color-vector 1 0 0 1))
(defparameter *color-green* (color-vector 0 1 0 1))
(defparameter *color-blue* (color-vector 0 0 1 1))
;; color-table
;; https://web.archive.org/web/20180301041827/https://prideout.net/archive/colors.php
(defparameter *color-yellow* (color-vector 1.0 1.0 0.0 1.0))
(defparameter *color-cyan* (color-vector 0.000 1.000 1.000))
(defparameter *color-lightcyan* (color-vector 0.878 1.000 1.000))
(defparameter *color-deepskyblue* (color-vector 0.000  0.749 1.000))
(defparameter *color-mediumpurple* (color-vector 0.576 0.439 0.859))


;; line

(defun project-line (v3-origin v3-delta &key (color *color-green*))
  (let* ((v3-to-at (vec3-add v3-origin v3-delta)))
    (gl:with-pushed-matrix
      (gl:disable :lighting)
      ;(gl:push-attrib :current-bit)
      (gl:with-primitives :lines
        (gl-color color)
        (gl:vertex (vec3-s1 v3-origin) (vec3-s2 v3-origin) (vec3-s3 v3-origin))
        (gl:vertex (vec3-s1 v3-to-at) (vec3-s2 v3-to-at) (vec3-s3 v3-to-at))
        ;;  (gl-color color-push)
        )
      (gl:enable :lighting)
      ;(gl:pop-attrib)
      )))

;;; edge 

(defun project-solid-edge (bottom height)
  ; useage example
  ; (gl:material :front-and-back :ambient-and-diffuse *color-green*)
  ; (project-solid-edge 0.05 edge-const)
  (let* ((bottom1 (vector-3d (- bottom) 0 0))
         (bottom2 (vector-3d (+ bottom) 0 0))
         (bottom3 (vector-3d 0 0 (- bottom)))
         (bottom4 (vector-3d 0 0 (+ bottom)))
         (top (vector-3d 0 height 0)))
    (gl:with-pushed-matrix
      ;; bottom
      (gl-primitive-triangle bottom1 bottom2 bottom4)
      (gl-primitive-triangle bottom2 bottom1 bottom3)
      ;; side
      (gl-primitive-triangle bottom1 top bottom3)
      (gl-primitive-triangle bottom2 top bottom4)
      (gl-primitive-triangle bottom3 top bottom2)
      (gl-primitive-triangle bottom4 top bottom1))))

(defun project-wire-edge (bottom height)
  (let* ((bottom1 (vector-3d (- bottom) 0 0))
         (bottom2 (vector-3d (+ bottom) 0 0))
         (bottom3 (vector-3d 0 0 (- bottom)))
         (bottom4 (vector-3d 0 0 (+ bottom)))
         (top (vector-3d 0 height 0)))
    (gl:with-pushed-matrix
      (gl:disable :lighting)
      ;; bottom
      (gl:with-primitive :line-strip
        (gl-vertex-by-vec3 bottom1) (gl-vertex-by-vec3 bottom4)
        (gl-vertex-by-vec3 bottom2) (gl-vertex-by-vec3 bottom3)
        (gl-vertex-by-vec3 bottom1))
      ;; side
      (gl:with-primitive :lines
        (gl-vertex-by-vec3 bottom1) (gl-vertex-by-vec3 top)
        (gl-vertex-by-vec3 bottom2) (gl-vertex-by-vec3 top)
        (gl-vertex-by-vec3 bottom3) (gl-vertex-by-vec3 top)
        (gl-vertex-by-vec3 bottom4) (gl-vertex-by-vec3 top))
      (gl:enable :lighting))))

;;; axis

(defun project-xyz-axis (&key (scale 1.00))
  (let* (;; after origin-point x-unit, y-unit, z-unit ...
         (edge-const 0.1)
         ;;
         (x-from (vec3-multiply *base-vec3-axis-x* (- scale)))
         (y-from (vec3-multiply *base-vec3-axis-y* (- scale)))
         (z-from (vec3-multiply *base-vec3-axis-z* (- scale)))
         ;;
         (x-to (vec3-multiply *base-vec3-axis-x* (* 2 scale)))
         (y-to (vec3-multiply *base-vec3-axis-y* (* 2 scale)))
         (z-to (vec3-multiply *base-vec3-axis-z* (* 2 scale))))
  (gl:with-pushed-matrix 
    ;;(gl:load-identity)    ;; world system
    (gl:disable :lighting)
    ;; X-Axis
    (project-line x-from x-to :color *color-red*)
    ;; X-edge
    (gl:with-pushed-matrix
      (gl-color *color-red*)
      (gl:translate (- scale edge-const) 0 0)
      (gl:rotate 90 0 0 -1)
      (project-wire-edge 0.05 edge-const)
      )
    ;; y-Axis
    (project-line y-from y-to :color *color-green*)
    ;; Y-edge
    (gl:with-pushed-matrix
      (gl-color *color-green*)
      (gl:translate 0 (- scale edge-const) 0)
      (gl:rotate 90 0 1 0)
      (project-wire-edge 0.05 edge-const))
    ;; Z-Axis
    (project-line z-from z-to :color *color-blue*)
    ;; Z-edge
    (gl:with-pushed-matrix
      (gl-color *color-blue*)
      (gl:translate 0 0 (- scale edge-const))
      (gl:rotate 90 1 0 0)
      (project-wire-edge 0.05 edge-const))
    ;;
    (gl:enable :lighting))))

;; Vector


(defun project-vector (coordinate-from delta &key (color *color-white*))
  (let* ((len (vec3-magnitude delta)))
    (when (> len 0)
      (let* ((degree (radian-to-degree (acos (/ (vec3-s3 delta) len)))))
             ;;(vec-to-at (vec3-add coordinate-from delta)))
        ;; line
        (project-line coordinate-from delta :color color)
        (gl:with-pushed-matrix
          (gl-translate-by-vec3 coordinate-from)
          (gl:rotate degree (- (vec3-s2 delta)) (vec3-s1 delta) 0)
          (gl:with-pushed-matrix
            (when color (gl-color color))
            (gl:translate 0 0 (- len 0.05))
            (gl:rotate 90 1 0 0)
            (project-wire-edge 0.02 0.05)))))))

;; text

;;(ql:quickload :glisph)

(defun draw-string (string x0 y0 &optional (z0 0))
  ;;string x0 y0 z0
  (gl:with-pushed-matrix
    (gl:disable :lighting)
    (%gl:raster-pos-3f (float x0) (float y0) (float z0))
    (glut:bitmap-string glut:+bitmap-9-by-15+ string)
    (gl:enable :lighting)))

(defun draw-string-3d (string vec3)
  (draw-string string (vec3-s1 vec3) (vec3-s2 vec3) (vec3-s3 vec3)))

(defun draw-string-2d (string relate-x relate-y &optional (x-max 1.00) (y-max 1.00))
  "0 < relate-x,y < x,y-max to inside of screen. (0, 0) is lefts bottom."
  (gl:matrix-mode :projection)
  (gl:with-pushed-matrix
    (gl:load-identity)
    (let* ((width (glut:get :window-width))
           (height (glut:get :window-height))
           (x (* width (/ relate-x x-max)))
           (y (* height (/ relate-y y-max))))
      width height
      (%gl:window-pos-2f (float x) (float y)))
    (glut:bitmap-string glut:+bitmap-9-by-15+ string)
    )
  (gl:matrix-mode :modelview))





