(in-package :physical-worlds)

;; template for gui frame updating

(defparameter *default-time-step* (/ 1 50))

(defclass timer-template ()
  ((frame-counter :initform 0)
   (current-time-step :initform 0)
   (sampled-time-at :initform 0)
   (sampled-frame-counter :initform 0)
   (configured-time-step :initform *default-time-step*)
   (counter-stoppingp :initform nil)
   ;;
   (applied-window :initform nil)
   (time-ago :initform 0)))

;;

(defun timestep-to-fps (timestep)
  (if (= timestep 0)
      0
      (/ 1000 timestep)))

(defmethod what-time-of ((tt timer-template))
  tt
  0)

(defmethod switch-call-or-stop-timer ((tt timer-template))
  (with-slots (counter-stoppingp) tt
    (setf counter-stoppingp (not counter-stoppingp))))


(defmethod get/set-timestep-params ((tt timer-template) frame-per-sample)
  (with-slots (sampled-time-at sampled-frame-counter frame-counter current-time-step) tt
    (let* ((current-time (what-time-of tt))
           (d_count (- frame-counter sampled-frame-counter))
           (d_time (- current-time sampled-time-at)))
      (when (>= d_count frame-per-sample)
        (setf current-time-step
              (if (zerop d_count)
                  0
                  (float (/ d_time d_count))))
        (setf sampled-frame-counter frame-counter)
        (setf sampled-time-at current-time)))
    current-time-step))


(defmethod format-timer-template-status ((tt timer-template))
  (with-slots (frame-counter configured-time-step counter-stoppingp time-ago) tt
    (concatenate 
     'string
     (format nil "frame counted: ~A~%" frame-counter)
     (format nil "timers-time counter: ~A~%" (what-time-of tt))
     (format nil "frame/second: ~A~%" 
             (timestep-to-fps (get/set-timestep-params tt 10)))
     (format nil "configured time step: ~A~%" configured-time-step)
     (format nil "time-ago: ~,4F~%" 
             time-ago)
     (format nil "stopping?: ~A~%" 
             counter-stoppingp)

)))
