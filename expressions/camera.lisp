(in-package :physical-worlds)

;; camera

(defclass euler-camera (domain-object)
  ((focussing
    :initarg :focussing
    :initform (vector-3d 0 0 0))
   (euler-xyz-angle
    :initarg :euler-xyz-angle
    :initform (euler-xyz-angle 0 0 0))
   (distance
    :initarg :distance
    :initform 10)
   (near 
    :initarg :near
    :initform 0.1)
   (far
    :initarg :far
    :initform 50)
   (theta-fovy
    :initarg :theta-fovy
    :initform 60.0)))

(defmethod format-camera-status ((cam euler-camera))
  (with-slots (position) cam
    position
    (let* ((str1 (describe-object-to-string cam))
           (look-from (look-from cam))
           (look-to (focussing-point cam))
           (dist-l (vec3-magnitude (vec3-sub look-to look-from)))
           (dist-o (vec3-magnitude (vec3-sub (vector-3d 0.0 0.0 0.0) look-from)))
           (forward (forward cam)))
      (format nil "~A~%from: ~A~%to: ~A~%dist-l, dist-o: ~A,~A~%, forward: ~A~%"
              str1 look-from look-to dist-l dist-o forward))))

(defmethod reperspect! ((c euler-camera) width height)
  (let ((near (slot-value c 'near))
        (far  (slot-value c 'far))
        (theta-fovy (slot-value c 'theta-fovy)))
    (gl:matrix-mode :projection)    
    (gl:load-identity)    
    (glu:perspective theta-fovy (/ width height) near far)
    (gl:matrix-mode :modelview)
    (gl:load-identity)))


(defmethod fovy-zoom ((c euler-camera) direction)
  (let ((fovy (slot-value c 'theta-fovy)))
    (setf (slot-value c 'theta-fovy)
          (cond ((and (> direction 0) (> fovy 15))
                 (* fovy (/ 1 1.05)))
                ((and (< direction 0) (< fovy 80)) 
                 (* fovy (* 1 1.05)))
                (t fovy))))
  (reperspect! c (glut:get :window-width) (glut:get :window-height)))

(defmethod focussing-point ((c euler-camera))
  (let (;(look-from (position c))
        (focuss (slot-value c 'focussing)))
    (cond ((typep focuss 'domain-object)
           (slot-value focuss 'position))
          ((vec3-p focuss)
           focuss)
          (t
           (progn
             (warn "type of focussing point is not better")
             (vector-3d 0 0 0))))))

(defmethod look-from ((c euler-camera))
  (with-slots (distance (angles euler-xyz-angle) (origin position)) c
    (let*
        ((unit (vector-3d 1 0 0))
         (look-to (focussing-point c))
         (look-from
           (matrix-vector-multiplication
            (matrix-multiplications
             (conversion-parallel-move (vec3-s1 look-to) (vec3-s2 look-to) (vec3-s3 look-to))
             ;;(conversion-rotate-tait-bryan-angle-xyz
             (conversion-rotate-euler-angle-xyz
              (euler-xyz-angle-roll angles)
              (euler-xyz-angle-pitch angles)
              (euler-xyz-angle-yaw angles)))
            (vec3-to-vec4
             (vec3-multiply unit distance)))))
      (setf origin look-from))))


(defmethod looks-up ((c euler-camera))
  (let ((unit (vec3-to-vec4 (vector-3d 0 1 0) 0))
        ;;(distance (slot-value c 'distance))
        (angles (slot-value c 'euler-xyz-angle)))
    (vec3-normalize
     (vec4-sss0-vec3
      (matrix-vector-multiplication
       (conversion-rotate-euler-angle-xyz
        (euler-xyz-angle-roll angles)
        (euler-xyz-angle-pitch angles)
        (euler-xyz-angle-yaw angles))
       unit)))))



(defmethod roll-camera ((c euler-camera) d_roll)
  (let ((euler-ago (slot-value c 'euler-xyz-angle)))
    (setf (slot-value c 'euler-xyz-angle)
          (euler-xyz-angle
           (+ d_roll (euler-xyz-angle-roll euler-ago))
           (euler-xyz-angle-pitch euler-ago)
           (euler-xyz-angle-yaw euler-ago)))))

(defmethod pitch-camera ((c euler-camera) d_pitch)
  (let ((euler-ago (slot-value c 'euler-xyz-angle)))
    (setf (slot-value c 'euler-xyz-angle)
          (euler-xyz-angle
           (euler-xyz-angle-roll euler-ago)
           (+ d_pitch (euler-xyz-angle-pitch euler-ago))
           (euler-xyz-angle-yaw euler-ago)))))

(defmethod yaw-camera ((c euler-camera) d_yaw)
  (let ((euler-ago (slot-value c 'euler-xyz-angle)))
    (setf (slot-value c 'euler-xyz-angle)
          (euler-xyz-angle
           (euler-xyz-angle-roll euler-ago)
           (euler-xyz-angle-pitch euler-ago)
           (+ d_yaw (euler-xyz-angle-yaw euler-ago))))))

(defmethod pitch ((c euler-camera) d_pitch)
  c d_pitch
)

(defmethod forward ((c euler-camera))
  (with-slots ((cam-coord position)) c
    (let ((focus-coord (focussing-point c)))
      (vec3-sub focus-coord cam-coord))))

(defmethod look ((c euler-camera))
  (let* ((focussing-point (focussing-point c))
         (look-from (look-from c))
         (looks-up (looks-up c)))
  (glu:look-at
   (vec3-s1 look-from) (vec3-s2 look-from) (vec3-s3 look-from)
   (vec3-s1 focussing-point) (vec3-s2 focussing-point) (vec3-s3 focussing-point)
   (vec3-s1 looks-up) (vec3-s2 looks-up) (vec3-s3 looks-up)
   )))

