(in-package :physical-worlds)

;; references:
;; http://marupeke296.com/COL_2D_No8_QuadTree.html
;; http://marupeke296.com/COL_2D_No9_QuadTree_Imp.html
;; http://marupeke296.com/COL_3D_No15_Octree.html
;; http://marupeke296.com/COLSmp_3D_No3_Octree.html



;;; division space

(defclass space-cell ()
  ; ccell
  ((point-to-new-obj :initform nil) ; pointer to newest object-for-tree
   
   )
  )

;;; object register. into the division space

(defclass object-for-tree ()
  ((registerd-space-cell :initarg :registerd-space-cell :initform (make-instance 'space-cell))
   ; doubly-linked list
   (current-object :initarg :detection-object :initform nil)
   (next-object-for-tree :initform nil ;(make-instance 'object-for-tree)
                         )
   (prev-object-for-tree :initform nil ; (make-instance 'object-for-tree)))
                         )))

;;; methods

(defmethod cell-reset ((cell space-cell) (sp object-for-tree))
  )

(defmethod cell-push ((cell space-cell) (sp object-for-tree))
)

(defmethod cell-on-remove ((cell space-cell) (remove-obj object-for-tree))
)




(defmethod remove-self ((obj-ft object-for-tree))
  )

(defmethod register-cell ((obj-ft object-for-tree) (cell space-cell))
  )



;;; 2D tree

#|
(defun liner-quaternary-tree-index (belong-level morton-number-of-level)
  ;; addtion number is sigma n=0 to (1- belong-level) of 4**n = 1/3*((4**belonging-level) - 1)
  (let ((addition (* 1/3 (- (expt 4 belong-level) 1))))
    (+ addition morton-number-of-level)))

(defun 2d-bit-sepalation32 (n)
  (declare ((unsigned-byte 16) n)
           ;((unsigned-byte 32) s))
           )
  ;; 16bit index
  ;; bbbb bbbb bbbb bbbb
  (let* ((s (logand (logior n (ash n 8)) #x00ff00ff)) ; 00ff = 0000 0000 1111 1111
         (s (logand (logior s (ash s 4)) #x0f0f0f0f)) ; 0f0f = 0000 1111 0000 1111
         (s (logand (logior s (ash s 2)) #x33333333)) ; 3333 = 0011 0011 0011 0011
         (s (logand (logior s (ash s 1)) #x55555555))); 5555 = 0101 0101 0101 0101
         
    s))

(defun 2d-morton-number (x y)
  ; #b xxxx xxxx, #b yyyy yyyy -> #b xyxy xyxy xyxy xyxy
  (logior (ash (2d-bit-sepalation32 x) 0)
          (ash (2d-bit-sepalation32 y) 1)))
|#

;;; 3D tree

(defclass liner-octree-manager ()
  ())




(defun liner-octree-index (belong-level morton-number-of-level)
  ;; additional is sigma (n=0) to (1- belogn-level) of 8**n = 1/7*((8**belonging-level) -1)
  (let ((additional (* 1/7 (- (expt 8 belong-level) 1))))
    (+ additional morton-number-of-level)))

(defun 3d-bit-sepalation24 (n)
  (declare ((unsigned-byte 8) n)
           ;((unsigned-byte 24) s)
           )
  ; 24bit returns. 8bit/dimension.
  ; 0000 0000 0000 0000 1111 1111
  (let* ((s (logand (logior n (ash n 8)) #x0000f00f)) ; 00f00f = 0000 0000 1111 0000 0000 1111
         (s (logand (logior s (ash s 4)) #x000c30c3)) ; 03c0c3 = 0000 0011 1100 0000 0011 1100
         (s (logand (logior s (ash s 2)) #x00249249))); 249249 = 0010 0100 1001 0010 0100 1001
    s))

(defun 3d-morton-number (x y z)
  ; #b xxxx xxxx, #b yyyy yyyy, #b zzzz zzzz -> #b xyzx yzxy zxyz  xyzx yzxy zxyz  xyzx yzxy zxyz
  ; (3*4)*3/3 = 8
  (logior (ash (3d-bit-sepalation24 x) 0)
          (ash (3d-bit-sepalation24 y) 1)
          (ash (3d-bit-sepalation24 z) 2)))

(defun coordinate-morton-element (vec3-coordinate vec3-minimal-space-unit)
  (3d-morton-number (floor (/ (vec3-s1 vec3-coordinate) (vec3-s1 vec3-minimal-space-unit)))
                    (floor (/ (vec3-s2 vec3-coordinate) (vec3-s2 vec3-minimal-space-unit)))
                    (floor (/ (vec3-s3 vec3-coordinate) (vec3-s3 vec3-minimal-space-unit)))))

;;; 

(defun morton-number-for-region (vec3-aabbcc-min vec3-aabbcc-max
                                 vec3-minimal-space-unit)
  (let* (; morton axis position of each coordinate
         (LT (coordinate-morton-element vec3-aabbcc-min vec3-minimal-space-unit))
         (RB (coordinate-morton-element vec3-aabbcc-max vec3-minimal-space-unit))
         ; level from tree's root
         (Def (logxor LT RB))
         ;;
         )
    Def))


