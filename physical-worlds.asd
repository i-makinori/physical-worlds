

(in-package :cl-user)
(defpackage physical-worlds-asd
  (:use :cl :asdf))
(in-package :physical-worlds-asd)


(asdf:defsystem physical-worlds
  :license nil
  :version "0.0.1"
  :description "Physical Worlds"
  :author "i-makinori"
  :depends-on (cffi cl-opengl cl-glu cl-glut)
  :components
  ((:module  "expressions"
    :components
    ((:file "package")
     (:file "utils")
     ;;
     (:file "linear-algebra")
     (:file "domain-objects")
     (:file "collision-detection")
     (:file "camera")
     (:file "physics")
     ;;
     (:file "gl-utils")
     (:file "timer-template")
     (:file "input-handler")
     (:file "simulator-window") 
     ;; tests
     (:file "test-movelight")
     (:file "test-handle-event")
     (:file "test-collisions")
     ;; implementing
     (:file "division-tree")
     (:file "test-division-tree")
     ;; example spring
     (:file "test-springs-propagation")
     (:file "test-springs-propagation-example-waves")
     ;; example rolling balls
     (:file "test-rollings-utils")
     (:file "test-rollings")
     ;;
     (:file "export-all-symbols")
))))

